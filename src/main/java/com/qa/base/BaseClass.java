package com.qa.base;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class BaseClass {

	public static WebDriver driver;
	public static EventFiringWebDriver eventdriver;
	// public static WebEventListener webEventListener;
	public static Properties properties;
	// public static JavascriptExecutor js;

	public BaseClass() {
		try {
			properties = new Properties();
			FileInputStream fileInputStream = new FileInputStream(
					"src\\main\\java\\com\\qa\\config\\config.properties");

			properties.load(fileInputStream);
			BasicConfigurator.configure();
			// js = (JavascriptExecutor) driver;
		} catch (Exception ex) {

		}

	}
	
	
	
	

	public static void initialization() {
		String browserName = properties.getProperty("browser");
		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					"src\\main\\java\\com\\qa\\driver78\\chromedriver.exe");
			driver = new ChromeDriver();
		}

		eventdriver = new EventFiringWebDriver(driver);
		// webEventListener=new WebEventListener();
		// eventdriver.register(webEventListener);
		driver = eventdriver;

		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		// driver.manage().timeouts().pageLoadTimeout(TestUtil.page_load_timeout,
		// TimeUnit.SECONDS);
		// driver.manage().timeouts().implicitlyWait(TestUtil.implicit_wait,
		// TimeUnit.SECONDS);
		driver.get(properties.getProperty("url"));

	}

}
