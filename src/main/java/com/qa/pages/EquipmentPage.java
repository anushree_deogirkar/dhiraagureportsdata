package com.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class EquipmentPage extends BaseClass {
	
//	Object Repo
	//*[@id="equipmentName"]
	
	@FindBy(id = "equipmentName")
	WebElement equipmentName;
	
	@FindBy(id = "equipmentMake")
	WebElement equipmentMake;
	
	@FindBy(id = "equipmentModel")
	WebElement equipmentModel;
	
	@FindBy(xpath = "//*[@id=\"equipmentTypeListDiv\"]/a[1]/span/span")
	WebElement equipmentTypeComboBox;
	
	@FindBy(xpath = "//*[@id=\"equipmentTypeListDiv\"]/div/span/input")
	WebElement equipmentTypeSearchBox;
	
	@FindBy(xpath = "//*[@id=\"equipmentDepartmentListDiv\"]/a[1]/span/span")
	WebElement organizatoinUnitComboBox;
	
	@FindBy(xpath = "//*[@id=\"equipmentDepartmentListDiv\"]/div/span/input")
	WebElement organizatoinUnitSearchBox;
	
	@FindBy(id = "serialNos")
	WebElement serialNo;
	
	@FindBy(id = "saveInfoBtn")
	WebElement saveButton;
	
	
	@FindBy(id = "searchEquipments")
	WebElement searchEquipment;
	
	public EquipmentPage() {
		PageFactory.initElements(driver, this);
	}

	public void SaveData(String name, String brand, String model, String type, String org, String serialNo2) throws InterruptedException {
		equipmentName.sendKeys(name);
		equipmentMake.sendKeys(brand);
		equipmentModel.sendKeys(model);
		equipmentTypeComboBox.click();
		Thread.sleep(1000);
		equipmentTypeSearchBox.sendKeys(type);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"equipmentTypeListDiv\"]/div/ul/li")).click();
		
		organizatoinUnitComboBox.click();
		Thread.sleep(1000);
		organizatoinUnitSearchBox.sendKeys(org);
		serialNo.sendKeys(serialNo2);
		
		
		
	}

	public void saveEquipment() {
		saveButton.click();
		
	}

	public void searchAddedEquipment(String name) {
		searchEquipment.sendKeys(name);
		
	}
			

}
