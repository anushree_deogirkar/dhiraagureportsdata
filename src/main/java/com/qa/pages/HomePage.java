package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class HomePage extends BaseClass{
//Object Repo
	
//	@FindBy(xpath = "//span[contains(text(),'Admin')]")
	@FindBy(xpath ="//*[@id='admin']/a")

	WebElement admin_button;

	@FindBy(xpath = "//a[contains(text(),'Organization Setup')]")

	WebElement organization_setup;

	@FindBy(xpath = "//li[@id='employee']/a")

	WebElement Team_Setup;

	@FindBy(xpath = "//a[contains(text(),'Manager')]")

	WebElement Manager_tab;
	
	@FindBy(xpath = "//*[@id=\"equipment\"]/a")

	WebElement Equipment_tab;

	@FindBy(xpath = "//span[contains(text(),'Tasks')]")

	WebElement Task_Menu;

	@FindBy(xpath = "/html/body/div[1]/header/nav/div[2]/ul[2]/li[3]/a/img")
	WebElement user_icon;

	@FindBy(xpath = "/html/body/div[1]/header/nav/div[2]/ul[2]/li[3]/ul/li/div/a[2]/div")
	WebElement logOut_button;
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	public void click_on_admin_button() {
		
		admin_button.click();
	}

	public void click_on_organization_setup() {
		organization_setup.click();
	}

	public void click_on_Team_Setup() {
		Team_Setup.click();
	}

	public void click_on_Manager() {
		Manager_tab.click();
	}
	
	public void click_on_Equipment() {
		Equipment_tab.click();
	}

	public void click_on_Task_Menu() {
		Task_Menu.click();
	}

	public void click_on_user_icon() {
		user_icon.click();
	}

	public void click_on_logOut_button() {
		logOut_button.click();
	}
}
