package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home_Page
{
WebDriver ldriver;
	
	public Home_Page(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver,this);
	}
	
	
		@FindBy(xpath="//span[contains(text(),'Admin')]")
		
		WebElement admin_button;
		
		@FindBy(xpath="//a[contains(text(),'Organization Setup')]")
		
		WebElement organization_setup;
		
		@FindBy(xpath="//li[@id='employee']/a")
		
		WebElement Team_Setup;
		
		
		@FindBy(xpath="//a[contains(text(),'Manager')]")
		
		WebElement Manager_tab;
		
		@FindBy(xpath="//span[contains(text(),'Tasks')]")
		
		WebElement Task_Menu;
		
		
		@FindBy(xpath="/html/body/div[1]/header/nav/div[2]/ul[2]/li[3]/a/img")		
		WebElement user_icon;
		
		
		@FindBy(xpath="/html/body/div[1]/header/nav/div[2]/ul[2]/li[3]/ul/li/div/a[2]/div")		
		WebElement logOut_button;
		
		
		
		
		public void click_on_admin_button()
		{
			admin_button.click();
		}
		
		
		public void click_on_organization_setup()
		{
			organization_setup.click();
		}
		
		
		public void click_on_Team_Setup()
		{
			Team_Setup.click();
		}
		
		public void click_on_Manager()
		{
			Manager_tab.click();
		}
		
		public void click_on_Task_Menu()
		{
			Task_Menu.click();
		}
		
		
		public void click_on_user_icon()
		{
			user_icon.click();
		}
		
		public void click_on_logOut_button()
		{
			logOut_button.click();
		}
		
	
		
		
		
		
		
		
	
		
}
