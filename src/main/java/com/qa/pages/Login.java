package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class Login extends BaseClass {

	// Object Repo

	@FindBy(id = "user") 
	@CacheLookup
	WebElement username_field;

	@FindBy(id = "password")
	@CacheLookup
	WebElement password_field;

	@FindBy(xpath = "//button[@class='btn btn-block btn-info-custom margin-top-04']")
	@CacheLookup
	WebElement login_button;

	public Login() {
		PageFactory.initElements(driver, this);
	}

	public void Enter_username(String username) {
		username_field.sendKeys(username);
	}

	public void Enter_password(String password) {
		password_field.sendKeys(password);
	}

	public HomePage login(String usernameNew,String passwordNew) throws InterruptedException {
//		username_field.sendKeys("Anushree123");
//		password_field.sendKeys("tech8092");
//		username_field.sendKeys("rahul.rajabhoj@locationguru.com");
//		password_field.sendKeys("tech8092");
		
		username_field.sendKeys("rajat.zade@locationguru.com");
		password_field.sendKeys("tech8092");
		login_button.click();
		
		return new  HomePage();
		
	}
}
