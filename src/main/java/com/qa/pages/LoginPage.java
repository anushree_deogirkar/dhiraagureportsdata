package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage
{
	WebDriver ldriver;
	
	public LoginPage(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver,this);
	}
	
	
		@FindBy(id="user")
		@CacheLookup
		WebElement username_field;
		
		
		@FindBy(id="password")
		@CacheLookup
		WebElement password_field;
		
		
		@FindBy(xpath="//button[@class='btn btn-block btn-info-custom margin-top-04']")
		@CacheLookup
		WebElement login_button;
		
		public void Enter_username(String username)
		{
			username_field.sendKeys(username);
		}
		
		
	
		public void Enter_password(String password)
		{
			password_field.sendKeys(password);
		}
		

		
		public void click_on_login_button()
		{
			login_button.click();
		}
		
		
		
		
		
		
	
		
		
		
		
		
		
		

}
