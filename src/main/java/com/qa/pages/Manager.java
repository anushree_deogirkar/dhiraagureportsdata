package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Manager
{
WebDriver ldriver;
	
	public Manager(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver,this);
	}
	
	
		@FindBy(xpath="//input[@type='button'][@value='Add']")
		
		WebElement Add_button_of_Manager;
		
		
		@FindBy(id="name")
		
		WebElement Manager_name_text_box;
		
		
		@FindBy(id="mobileNumber")
		
		WebElement Manager_mobile_number_text_box;
		
		
		@FindBy(id="email")
		
		WebElement Manager_email_id_text_box;
		
		
		@FindBy(id="username")
		
		WebElement Manager_username;
		
		
		@FindBy(id="selectZoneDropdownTreeBtn")
		
		WebElement select_organization_unit_dropdown;
		
		
		@FindBy(id="tree_search")
		
		WebElement manager_search_box;
		
		
		@FindBy(id="addManagerBtn")
		
		WebElement save_button_of_manager;
		
		
		
		@FindBy(xpath="//table[@id='managerTable']//tbody/tr[1]/td[2]/div[1]/span[1]")
		
		WebElement name_of_the_manager;
		
		
		@FindBy(xpath="//input[@placeholder='Search']")
		
		WebElement search_box;
		
		
		
		@FindBy(id="modifyButton")		
		WebElement manager_modify_button;
		
		
		@FindBy(id="modifyManagerBtn")		
		WebElement manager_update_button;
		
		@FindBy(id="deleteButton")		
		WebElement manager_delete_button;
		
		@FindBy(xpath="//button[@class='confirm']")		
		WebElement manager_confirmation_button;
		
		
		
		
		
		
		
		
		
		
		public void click_on_add_button_of_manager()
		{
			Add_button_of_Manager.click();
		}
		
		
		public void enter_manager_name(String name)
		{
			Manager_name_text_box.sendKeys(name);
		}
		
		
		public void clear_manager_name()
		{
			Manager_name_text_box.clear();
		}
		
		
		public void enter_manager_mobile_number(String mobileNumber)
		{
			Manager_mobile_number_text_box.sendKeys(mobileNumber);
		}
		
		public void enter_manager_emailid(String email)
		{
			Manager_email_id_text_box.sendKeys(email);
		}
		
		public void enter_manager_username(String username)
		{
			Manager_username.sendKeys(username);
		}
		
		
		
		public void click_on_select_organization_unit_dropdown()
		{
			select_organization_unit_dropdown.click();
		}
		
		
		
		public void enter_organization_unit(String organization_unit)
		{
			manager_search_box.sendKeys(organization_unit);
		}
		
		public void click_on_manager_name()
		{
			Manager_name_text_box.click();
		}
		
		public void click_on_save_button()
		{
			save_button_of_manager.click();
		}
		
		public String  capture_manager_name()
		{
			String manager_name = name_of_the_manager.getText();
			return manager_name;
		}
		
		
		public void enter_manager_name_in_search_box(String name)
		{
			search_box.sendKeys(name);
		}
		
		
		public void click_on_manager_modify_button()
		{
			manager_modify_button.click();
		}
		
		public void click_on_manager_update_button()
		{
			manager_update_button.click();
		}
		
		public void click_on_manager_delete_button()
		{
			manager_delete_button.click();
		}
		
		
		public void click_on_manager_confirmation_button()
		{
			manager_confirmation_button.click();
		}
		
		
		
}
