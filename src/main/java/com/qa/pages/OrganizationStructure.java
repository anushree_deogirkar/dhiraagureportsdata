package com.qa.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.base.BaseClass;

public class OrganizationStructure extends BaseClass {
	public static WebDriverWait wait = new WebDriverWait(driver, 30);

	WebDriver ldriver;

	@FindBy(id = "addDepartmentCentre")

	WebElement add_button_of_organization_structure;

	@FindBy(id = "centerId")

	WebElement unitIdTextBox;

	@FindBy(id = "centerName")

	WebElement unitNameTextBox;

	@FindBy(id = "address")

	WebElement unitAddressTextBox;

	@FindBy(id = "mapLat")

	WebElement Latitude_textbox;

	@FindBy(id = "maplong")

	WebElement Longitude_textbox;

	@FindBy(id = "saveServiceCenterBtn")

	WebElement Organization_Structure_Save_Button;
	
	@FindBy(id = "organizationStructureImportBtn")

	WebElement organization_Structure_Import_Button;

	@FindBy(id = "taskPerEngineer")

	WebElement minimum_task_per_day_textbox;

	@FindBy(xpath = "//div[@id='department-groups-div']//div//button[contains(@class,'ms-choice')]")

	WebElement organization_unit_type_dropdown;

	@FindBy(xpath = "//div[@id='department-list-div']//div//button[contains(@class,'ms-choice')]//div")
	WebElement parent_organization_unit_dropdown;

	@FindBy(xpath = "//button[@class='confirm']")
	WebElement confirmation_popup;

	@FindBy(xpath = "//label[contains(text(),'Division(D111)')]//input[@name='selectItemdepartment-groups']")
	WebElement select_division_from_organization_unit_type_dropdown;

	@FindBy(xpath = "//div[@id='department-groups-div']//div//ul/li[3]/label/input")
	WebElement select_department_from_organization_unit_type_dropdown;

	@FindBy(xpath = "//div[@id='addDepartmentBlock']//li[4]/label/input")
	WebElement select_section_from_organization_unit_type_dropdown;

	@FindBy(xpath = "//label[contains(text(),'Unit(U111)')]//input[@name='selectItemdepartment-groups']")
	WebElement select_unit_from_organization_unit_type_dropdown;

	@FindBy(xpath = "//div[@id='department-list-div']//li[2]/label/input")

	WebElement select_zone_from_parent_organization_unit;

	@FindBy(xpath = "//label[contains(text(),'Networks Division(6003.0)')]//input[@name='selectItemdepartment-list']")

	WebElement select_networks_division_from_parent_organization_unit;

	@FindBy(xpath = "//button[@class='confirm']")

	WebElement confirmation_popUp;

	@FindBy(xpath = "//div[@id='departmentsTab']//tbody//tr[1]/td[1]/span")

	WebElement recently_added_organization_name;

	@FindBy(xpath = "//div[@id='serviceCentreTable_filter']//input[@placeholder='Search']")

	WebElement search_box_of_organization_structure;

	@FindBy(xpath = "//span[@class='fa fa-ellipsis-h']")

	WebElement elipses_of_delta_division;

	@FindBy(xpath = "//button[@id='modifyDepartmentCentre']")

	WebElement modify_icon;

	@FindBy(id = "deleteCentre")
	WebElement delete_icon;

	@FindBy(xpath = "//div[@id='department-groups-div']//div//ul/li/*")
	WebElement list_of_elements_from_org_unit_type_dropdown;

	@FindBy(xpath = "//select[@name='serviceCentreTable_length']")
	WebElement records_per_page_dropdown;

	@FindBy(xpath = "//select[@name='serviceCentreTable_length']/option[2]")
	WebElement twenty_five_record_from_records_per_page_dropdown;

	public OrganizationStructure() {
		PageFactory.initElements(driver, this);
	}

	public void click_on_add_button_of_organization_structure() {
		add_button_of_organization_structure.click();
	}

	public void enter_organization_unit_id(String id) {
		unitIdTextBox.sendKeys(id);
	}

	public void enter_organization_unit_name(String unit_name) {
		unitNameTextBox.sendKeys(unit_name);
	}

	public void click_organization_unit_name() {
		unitNameTextBox.click();
	}

	public void clear_organization_unit_name() {
		unitNameTextBox.clear();
	}

	public void enter_organization_unit_address(String Address) {
		unitAddressTextBox.sendKeys(Address);
	}

	public void clear_Latitude_textbox() {
		Latitude_textbox.clear();
	}

	public void enter_lattitude(String lattitude) {
		Latitude_textbox.sendKeys(lattitude);
	}

	public void clear_Longitude_textbox() {
		Longitude_textbox.clear();
	}

	public void enter_Longitude(String Longitude) {
		Longitude_textbox.sendKeys(Longitude);
	}

	public void click_on_organization_structure_save_button() {
		Organization_Structure_Save_Button.click();

	}

	public void click_on_organization_unit_type_dropdown() {
		organization_unit_type_dropdown.click();
	}

	public void select_department_from_organization_unit_type_dropdown() {
		select_department_from_organization_unit_type_dropdown.click();
	}

	public void select_zone_from_parent_organization_unit() {
		select_zone_from_parent_organization_unit.click();
	}

	public void select_division_from_organization_unit_type_dropdown() {
		select_division_from_organization_unit_type_dropdown.click();
	}

	public void click_on_parent_organization_unit_dropdown() {
		parent_organization_unit_dropdown.click();
	}

	public void select_networks_division_from_parent_organization_unit() {
		select_networks_division_from_parent_organization_unit.click();
	}

	public void enter_minimum_task_per_day_textbox(String task) {
		minimum_task_per_day_textbox.sendKeys(task);
	}

	public void click_yes_button_of_confirmation_popUp() {
		confirmation_popUp.click();
	}

	public String recently_added_organization_name() {
		String orhanization_name = recently_added_organization_name.getText();

		return orhanization_name;
	}

	public void enter_recently_added_division_name(String search) {
		search_box_of_organization_structure.sendKeys(search);
	}

	public void click_on_elipses() {
		elipses_of_delta_division.click();
	}

	public void click_on_modify_icon() {
		modify_icon.click();
	}

	public void click_on_delete_icon() {
		delete_icon.click();
	}

	public void select_section_from_organization_unit_type_dropdown() {
		select_section_from_organization_unit_type_dropdown.click();
	}

	public void select_unit_from_organization_unit_type_dropdown() {
		select_unit_from_organization_unit_type_dropdown.click();
	}

	public void wait_until_the_organization_unit_type_dropdow_is_visible() {
		wait.until(ExpectedConditions.visibilityOf(organization_unit_type_dropdown));
	}

	public void wait_until_the_parent_organization_unit_dropdow_is_visible() {
		wait.until(ExpectedConditions.visibilityOf(parent_organization_unit_dropdown));
	}

	public void wait_until_the_confirmation_popup_is_visible() {
		wait.until(ExpectedConditions.visibilityOf(confirmation_popup));
	}

	public void wait_until_the_search_box_of_organization_structure_is_visible() {
		wait.until(ExpectedConditions.visibilityOf(search_box_of_organization_structure));
	}

	public void click_on_records_per_page_dropdown() {
		records_per_page_dropdown.click();
	}

	public void click_on_twenty_five_record_from_records_per_page_dropdown() {
		twenty_five_record_from_records_per_page_dropdown.click();
	}

	public void saveDivision(String unitId, String unitName, String address) {
		System.out.println("Hello");
		
		unitIdTextBox.sendKeys(unitId);
		unitNameTextBox.sendKeys(unitName);
		unitAddressTextBox.sendKeys(address);
//		minimum_task_per_day_textbox.sendKeys(tasksPerDay);
		organization_unit_type_dropdown.click();
	}

	public void setSearchData(String del) {
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		search_box_of_organization_structure.sendKeys(del);
		
	}

	public void click_on_import_button() {
		organization_Structure_Import_Button.click();
		
	}

}
