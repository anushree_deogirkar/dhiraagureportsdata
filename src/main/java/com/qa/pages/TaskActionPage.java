package com.qa.pages;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.base.BaseClass;

public class TaskActionPage extends BaseClass {
	public static WebDriverWait wait = new WebDriverWait(driver, 30);

	@FindBy(xpath = "//a[contains(text(),'Task Actions')]")
	WebElement Task_Action_Tab;

	@FindBy(id = "addJobActionsBtn")
	WebElement Add_button_of_Task_Action;

	@FindBy(id = "jobActionName")
	WebElement task_action_name_text_box;

	@FindBy(id = "becnmarktimetoComplete")
	WebElement benchmark_time_text_box;

	@FindBy(xpath = "//div[@class='departmentFilterblock']//button[contains(@class,'ms-choice')]")
	WebElement select_organization_unit_dropdown;

	@FindBy(xpath = "//div[@id='jobActionsTable_filter']//input[@placeholder='Search']")
	WebElement search_box_of_select_organization_unit_dropdown;

	@FindBy(xpath = "//button[@id='savejobActionsBtn']")
	WebElement save_button_of_task_action;

	@FindBy(xpath = "//div[@id='jobActionsTable_filter']//input[@class='form-control input-sm']")
	WebElement seach_box_of_task_action_page;

	@FindBy(id = "modifyJobActions")
	WebElement modify_pencil_of_task_actions;

	@FindBy(xpath = "//button[@id='deleteJobAction']//span[@class='fa fa-trash']")
	WebElement delete_pencil_of_task_actions;

	@FindBy(xpath = "//button[@class='confirm']")
	WebElement confirmation_popUp_for_delete;

	public TaskActionPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void click_on_Task_Action_Tab() {
		Task_Action_Tab.click();
	}

	public void click_on_Add_button_of_Task_Action() {
		Add_button_of_Task_Action.click();
	}

	public void enter_task_action_name(String name) {
		task_action_name_text_box.sendKeys(name);
	}

	public void enter_benchmark_time(String benchmark_time) {
		benchmark_time_text_box.sendKeys(benchmark_time);
	}

	public void click_on_select_organization_unit_dropdown() {
		select_organization_unit_dropdown.click();
	}

	public void enter_value_in_search_box_of_select_organization_unit_dropdown(String value) {
		search_box_of_select_organization_unit_dropdown.sendKeys(value);
	}

	public void click_on_save_button_of_task_action() {
		save_button_of_task_action.click();
	}

	public void enter_value_in_seach_box_of_task_action_page(String value1) {
		search_box_of_select_organization_unit_dropdown.sendKeys(value1);
	}

	public void wait_until_seach_box_of_task_action_page_is_visible() {

//		wait.until(ExpectedConditions.invisibilityOfElementLocated(seach_box_of_task_action_page));
	}

	public void click_on_task_action_name_text_box() {
		task_action_name_text_box.click();
	}

	public void clear_task_action_name_text_box() {
		task_action_name_text_box.clear();
	}

	public void click_on_modify_pencil_of_task_actions() {
		modify_pencil_of_task_actions.click();
	}

	public void click_on_delete_pencil_of_task_actions() {
		delete_pencil_of_task_actions.click();
	}

	public void click_on_confirmation_popUp_for_delete() {
		confirmation_popUp_for_delete.click();
	}

	public void saveTask(String name, String time) {
		
		
		
	task_action_name_text_box.sendKeys(name);
	benchmark_time_text_box.sendKeys(time.toString());
		
	}

}
