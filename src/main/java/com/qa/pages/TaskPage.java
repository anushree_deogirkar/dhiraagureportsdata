package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class TaskPage extends BaseClass {
	@FindBy(id="taskOptions-add")		
	WebElement add_button_of_task;
	
	
	
	@FindBy(xpath="//div[@id='serviceCentreId']//a[@class='dropdown-display']")	
	WebElement organization_unit_dropdown;
	
	
	@FindBy(xpath="//div[@id='serviceCentreId']//input")	
	WebElement search_of_organization_unit_dropdown;
	
	@FindBy(id="order-desc")		
	WebElement title_text_box;
	
	@FindBy(xpath="//div[@id='taskTypeId']//span[@class='placeholder']")	
	WebElement type_dropdown;
	
	@FindBy(xpath="//div[@id='taskTypeId']//input")	
	WebElement search_box_of_type_drop_down;
	
	
	@FindBy(id="clientServiceOrderId")	
	WebElement order_number_text_box;
	
	@FindBy(xpath="//div[@class='col-xs-12 margin-top-1']//input[@id='clientName'][@name='clientName']")	
	WebElement customer_name_text_box;
	
	
	@FindBy(id="contactNumber")	
	WebElement customer_contact_number_text_box;
	
	
	@FindBy(xpath="//div[@id='stateMultiselectId']//span[@class='placeholder']")	
	WebElement state_dropdown;
	
	@FindBy(xpath="//div[@id='stateMultiselectId']//input")	
	WebElement search_box_of_state_dropdown;
		
	
	@FindBy(xpath="//div[@id='cityMultiselectId']//span[@class='placeholder']")	
	WebElement city_dropdown;
	
	@FindBy(xpath="//div[@id='cityMultiselectId']//input")	
	WebElement search_box_of_city_dropdown;
	
	@FindBy(xpath="//*[@id=\"taskStatus-600\"]")	
	WebElement assignedTab;
	
	
	
	
	
	@FindBy(id="address")	
	WebElement address_text_box;
	
	@FindBy(xpath="//div[@class='form-group mt margin-left-1-5']/input[@id='taskAddBtn']")	
	WebElement save_button_of_task_page;
	
	
	@FindBy(xpath="//li[@id='taskListView']")	
	WebElement tile_view;
	
	
	@FindBy(xpath="//li[@id='taskTabelView']")	
	WebElement list_view;
	
	@FindBy(xpath="//span[@class='task-list-options fa fa-ellipsis-h']")	
	WebElement three_horizontal_dots;
	
	
	@FindBy(xpath="//input[@id='taskList-search-bar']")	
	WebElement search_box_of_task_page;
	
	@FindBy(xpath="//div[contains(text(),'View Details')]")	
	WebElement view_detail;
	
	@FindBy(xpath="//*[@id='multipleSelect-assignedTo-taskList-modal-apply']")	
	WebElement assignedButton;
	
	
	@FindBy(id="taskStatus-all")	
	WebElement All_button;
	
	@FindBy(xpath="//i[@class='fa fa-pencil absolute-edit-icon clickable']")	
	WebElement quick_assign_pencil;
	
	
	@FindBy(xpath="//span[@class='dropdown-selected']")	
	WebElement select_member_dropdown;
	
	
	@FindBy(xpath="//div[@id='multipleSelect-assignedTo-taskList-modal']//span[contains(@class,'dropdown-search')]")	
	WebElement search_box_of_select_member_dropdown;
	
	@FindBy(id="multipleSelect-assignedTo-taskList-modal-apply")	
	WebElement quick_task_assignment_button;
	
	
	@FindBy(id="taskStatus-755")	
	WebElement Assigned_button;
	
	@FindBy(xpath="//span[@class='fa fa-pencil tasklist-quick-action-icon tasklist-quick-pencil-icon']")	
	WebElement edit_button_of_task;
	
	@FindBy(id="taskUpdateBtn")	
	WebElement update_button;
	
	
	@FindBy(xpath="//button[@class='confirm']")	
	WebElement confirmation_button_of_modify;
	
	@FindBy(xpath="//span[@title='Delete Task']")	
	WebElement delete_button;
	
	@FindBy(xpath="//button[@class='confirm']")	
	WebElement confirmation_button_of_delete;
	
	
	@FindBy(xpath="//*[@id='multipleSelect-assignedTo-taskList-modal']/div[2]/ul/li")
	
	WebElement selectAssignee;
	
	public TaskPage() {
		PageFactory.initElements(driver, this);
	}

	
	
	public void click_on_add_button_of_task()
	{
		add_button_of_task.click();
	}
	
	public void click_on_selectAssignee()
	{
		selectAssignee.click();
	}
	
	public void click_onAssigned()
	{
		assignedTab.click();
	}
	
	public void click_on_organization_unit_dropdown()
	{
		organization_unit_dropdown.click();
	}
	
	public void enter_org_unit_in_organization_unit_dropdown(String org)
	{
		search_of_organization_unit_dropdown.sendKeys(org);
	}

	
	public void enter_title_in_title_text_box(String title)
	{
		title_text_box.sendKeys(title);
	}
	
	public void click_on_type_dropdown()
	{
		type_dropdown.click();
	}
	
	
	public void enter_type_in_search_box_of_type_drop_down(String type)
	{
		search_box_of_type_drop_down.sendKeys(type);
	}
	
	
	public void enter_order_number_in_order_number_text_box(String ord)
	{
		order_number_text_box.sendKeys(ord);
	}
	
	public void enter_customer_name_in_customer_name_text_box(String name)
	{
		customer_name_text_box.sendKeys(name);
	}
	
	
	public void clear_customer_name_in_customer_name_text_box()
	{
		customer_name_text_box.clear();
	}
	
	public void enter_customer_contact_number_in_customer_contact_number_text_box(String num)
	{
		customer_contact_number_text_box.sendKeys(num);
	}
	
	public void click_on_state_dropdown()
	{
		state_dropdown.click();
	}
	
	
	public void enter_state_in_search_box_of_state_dropdown(String state)
	{
		search_box_of_state_dropdown.sendKeys(state);
	}
	
	public void click_on_city_dropdown()
	{
		city_dropdown.click();
	}
	
	public void enter_city_in_search_box_of_city_dropdown(String city)
	{
		search_box_of_city_dropdown.sendKeys(city);
	}
	
	
	public void enter_address_in_address_text_box(String addr)
	{
		address_text_box.sendKeys(addr);
	}
	
	public void save_button_of_task_page()
	{
		save_button_of_task_page.click();
	}
	
	public void click_on_tile_view()
	{
		tile_view.click();
	}
	
	public void click_on_list_view()
	{
		list_view.click();
	}
	
	public void enter_order_number_in_search_box_of_task_page(String ord_number)
	{
		search_box_of_task_page.sendKeys(ord_number);
	}
	
	public void click_on_three_horizontal_dots()
	{
		three_horizontal_dots.click();
	}
	
	public void click_on_view_detail()
	{
		view_detail.click();
	}
	
	
	public void click_on_All_button()
	{
		All_button.click();
	}
	
	
	public void click_on_quick_assign_pencil()
	{
		quick_assign_pencil.click();
	}
	
	public void click_on_select_member_dropdown()
	{
		select_member_dropdown.click();
	}
	
	public void enter_team_member_name_in_search_box_of_select_member_dropdown(String team_member)
	{
		search_box_of_select_member_dropdown.sendKeys(team_member);
	}
	
	public void click_on_quick_task_assignment_button()
	{
		quick_task_assignment_button.click();
	}
	
	
	public void click_on_Assigned_button()
	{
		Assigned_button.click();
	}
	
	public void click_on_edit_button_of_task()
	{
		edit_button_of_task.click();
	}
	public void click_on_update_button_of_task()
	{
		update_button.click();
	}
	
	public void click_on_confirmation_button_of_modify()
	{
		confirmation_button_of_modify.click();
	}
	
	
	public void click_on_delete_button()
	{
		delete_button.click();
	}
	
	public void click_on_confirmation_button_of_delete()
	{
		confirmation_button_of_delete.click();
	}



	public void clickOnAssign() {
		assignedButton.click();
		
	}
}
