package com.qa.pages;

import java.math.RoundingMode;
import java.text.DecimalFormat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class TaskTypePage  extends BaseClass{

	WebDriver ldriver;

	


	@FindBy(xpath="//a[contains(text(),'Task Type')]")

	WebElement Task_Type_Tab;


	@FindBy(id="addJobType")

	WebElement Add_button_of_task_type;

	@FindBy(id="taskTypeName")

	WebElement Task_type_name_text_box;


	@FindBy(id="taskTypeId")

	WebElement Task_type_id_text_box;

	@FindBy(xpath="//div[@class='form-group']//button[contains(@class,'ms-choice')]")

	WebElement Task_group_dropdown;


	@FindBy(xpath="//div[@class='form-group']//div[@class='ms-search']//input")

	WebElement Task_group_dropdown_search;


	@FindBy(id="saveJobTypeBtn")
	WebElement save_button_of_task_type;


	@FindBy(xpath="//div[@id='jobTypesTable_filter']//input[@placeholder='Search']")
	WebElement task_type_page_search;



	@FindBy(xpath="//table[@id='jobTypesTable']//tbody/tr[1]/td[1]")
	WebElement recently_added_task_type_name;


	@FindBy(xpath="//label[contains(text(),'FIBRE-BB')]//input[@name='selectItem']")
	WebElement fiberBB_option_from_task_grup_dropdown;


	@FindBy(xpath="//table[@id='jobTypesTable']//span[@class='fa fa-ellipsis-h']")
	WebElement elipses_of_task_type;

	
	@FindBy(id="modifyJobTypes")
	WebElement modify_icon;
	
	@FindBy(xpath="//*[@id=\"slaTime\"]")
	WebElement slaTime;
	
	@FindBy(xpath="//*[@id=\"avgCompletionTime\"]")
	WebElement tatTime;
	
	
	@FindBy(xpath="//table[@id='jobTypesTable']//label")
	WebElement delete_elipses_of_task_type;
	
	
	@FindBy(xpath="//button[@id='deleteJobTypes']")
	WebElement delete_delete_icon_of_task_type;
	
	
	
	public TaskTypePage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void click_on_task_type_tab()
	{
		Task_Type_Tab.click();
	}

	public void click_on_Add_button_of_Task_Type()
	{
		Add_button_of_task_type.click();
	}

	public void enter_task_type_name(String name)
	{
		Task_type_name_text_box.sendKeys(name);
	}

	public void enter_task_type_id(String id)
	{
		Task_type_id_text_box.sendKeys(id);
	}


	public void click_on_Task_group_dropdown()
	{
		Task_group_dropdown.click();

	}

	public void enter_task_group_name_in_task_group_search_box(String task_group_search)
	{
		Task_group_dropdown_search.sendKeys(task_group_search);
	}

	public void click_on_Save_button()
	{
		save_button_of_task_type.click();

	}


	public void enter_recently_added_task_type_name(String name_of_task_type)
	{
		task_type_page_search.sendKeys(name_of_task_type);

	}


	public String recently_added_task_type_name()
	{
		String task_type_name = recently_added_task_type_name.getText();

		return  task_type_name;
	}

	public void select_fiberBB_option_from_task_grup_dropdown()
	{
		fiberBB_option_from_task_grup_dropdown.click();

	}

	
	public void click_on_elipses()
	{
		elipses_of_task_type.click();
	}

	
	public void click_on_modify_icon()
	{
		modify_icon.click();
	}
	
	
	public void click_on_delete_elipses_of_task_type()
	{
		delete_elipses_of_task_type.click();
	}
	
	public void click_on_delete_delete_icon_of_task_type()
	{
		delete_delete_icon_of_task_type.click();
	}


	public void saveTaskTypeData(String name, String id, String group) {
		Task_type_name_text_box.sendKeys(name);
		Task_type_id_text_box.sendKeys(id);
		
		

	}


	public void addData(String name, String id, String group) {
		Task_type_name_text_box.sendKeys(name);
		Task_type_id_text_box.sendKeys(id);
		
	}
	
	
}
