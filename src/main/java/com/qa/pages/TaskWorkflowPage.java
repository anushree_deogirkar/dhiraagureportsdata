package com.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.qa.base.BaseClass;

public class TaskWorkflowPage extends BaseClass {
	@FindBy(xpath="//a[contains(text(),'Task Workflows')]")
	WebElement Task_Workflow_Tab;
	
	@FindBy(id="addNewWorkBtn")
	WebElement Add_new_workflow_button;
	
	@FindBy(id="taskWorkFlowName")
	WebElement task_workflow_name_text_box;
	
	@FindBy(xpath="//*[@id=\"taskTypeBlock\"]/div/button/span")
	WebElement task_type_dropdown;
	
	
	@FindBy(xpath="//div[@id='taskTypeBlock']//div[@class='ms-search']//input")
	WebElement search_box_of_task_type_dropdown;
	
	
	@FindBy(xpath="//span[contains(text(),'Select City For State')]")
	WebElement state_cities_dropdown;
	
	
	@FindBy(xpath="//div[@id='selectStateCityBlock']//div[@class='ms-search']//input")
	WebElement state_cities_search_box;
	
	
	@FindBy(id="addMoreMessage")
	WebElement Add_organization_unit;
	
	
	@FindBy(xpath="//span[contains(text(),'Select Organization Unit')]")
	WebElement select_organization_unit_dropdown;
	
	
	@FindBy(xpath="//div[@class='workFlDepartmentFilterblock']//div[@class='ms-search']//input")
	WebElement searchbox_of_select_organization_unit_dropdown;
	
	
	
	@FindBy(name="avgCompletionTimeOfDept")
	WebElement average_completion_time;
	
	
	@FindBy(xpath="//button[@id='saveTaskWorkflowBtn']")
	WebElement save_button_of_task_workflow;
	
	
	@FindBy(xpath="//div[@id='workFlowTable_filter']//input[@placeholder='Search']")
	WebElement searchbox_of_task_workflow;
	
	
	@FindBy(id="updateTaskWorkflowBtn")
	WebElement update_button_of_task_workflow;
	
	@FindBy(xpath="//button[@class='confirm']")
	WebElement delete_confirmation_button;
	
	public TaskWorkflowPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	public void click_on_Task_Workflow_Tab()
	{
		Task_Workflow_Tab.click();
	}
	
	public void click_on_Add_new_workflow_button(String name)
	{
		Add_new_workflow_button.click();
		task_workflow_name_text_box.sendKeys(name);
	}
	
	public void enter_task_workflow_name_text_box(String workflow_name)
	{
		task_workflow_name_text_box.sendKeys(workflow_name);
	}
	
	public void clear_task_workflow_name_text_box()
	{
		task_workflow_name_text_box.clear();
	}
	
	public void click_on_task_workflow_name()
	{
		task_workflow_name_text_box.click();
	}
	
	
	
	public void click_on_task_type_dropdown()
	{
		WebDriverWait wait = new WebDriverWait(driver, 30);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\\\"taskTypeBlock\\\"]/div/button/span")));

		task_type_dropdown.click();
	}
	
	public void enter_task_type_name_in_search_box_of_task_type(String task_type)
	{
		search_box_of_task_type_dropdown.sendKeys(task_type);
	}
	
	public void click_on_state_cities_dropdown()
	{
		state_cities_dropdown.click();
	}
	
	
	public void enter_state_and_city_in_state_cities_search_box(String workflow_name)
	{
		state_cities_search_box.sendKeys(workflow_name);
	}
	
	
	public void click_on_add_organization_unit_button()
	{
		Add_organization_unit.click();
	}
	
	
	public void click_on_select_organization_unit_dropdown()
	{
		select_organization_unit_dropdown.click();
	}
	
	public void enter_organization_unit_in_searchbox_of_select_organization_unit_dropdown(String org_unit)
	{
		searchbox_of_select_organization_unit_dropdown.sendKeys(org_unit);
	}
	
	
	public void enter_average_completion_time_in_average_completion_time(String avg_time)
	{
		average_completion_time.sendKeys(avg_time);
	}
	
	public void click_on_average_completion_time()
	{
		average_completion_time.click();
	}
	
	public void click_on_save_button_of_task_workflow()
	{
		save_button_of_task_workflow.click();
	}
	
	public void enter_workflow_name_which_need_to_be_searched(String workflow_name)
	{
		searchbox_of_task_workflow.sendKeys(workflow_name);
	}
	
	public void click_on_update_button_of_task_workflow()
	{
		update_button_of_task_workflow.click();
	}
	
	public void click_on_delete_confirmation_button()
	{
		delete_confirmation_button.click();
	}
	

}
