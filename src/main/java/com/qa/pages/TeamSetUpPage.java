package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.base.BaseClass;

public class TeamSetUpPage extends BaseClass{

	WebDriver ldriver;
	
	@FindBy(xpath="//input[@class='btn mb-sm  btn-info'][@value='Add']") 
	WebElement add_button_of_team_setup;

	@FindBy(id="employeeName") 
	WebElement team_member_text_box;
	
	@FindBy(id="email") 
	WebElement email;
	
	@FindBy(id="mobNumber") 
	WebElement mobile_number;
	
	@FindBy(id="selectedCenterId") 
	WebElement select_organization_unit_dropdown;
	
	@FindBy(xpath="//div[@id='filter-shifts-wrapper']//button[contains(@class,'ms-choice')]") 
	WebElement select_shift_dropdown;
	
	@FindBy(xpath="//span[contains(text(),'Select Designation')]") 
	WebElement select_designation_dropdown;
	
	@FindBy(xpath="//span[@class='placeholder']") 
	WebElement select_skill_dropdown;
	
	@FindBy(id="addEmployeeBtn") 
	WebElement save_button_of_team_setup;
	
	@FindBy(id="tree_search") 
	WebElement search_box_of_organization_unit_dropdown;
	
	@FindBy(id="BPID") 
	WebElement employee_id;
	
	@FindBy(xpath="//div[@id='filter-shifts-wrapper']//li[1]/label/input[1]") 
	WebElement first_shift_of_select_shift_dropdown;
	
	
	@FindBy(xpath="//input[@class='form-control input-sm']") 
	WebElement search_box_of_team_setup_page;
	
	
	@FindBy(xpath="//table[@id='teamTable']//tbody//td[2]/div[1]/span[1]") 
	WebElement name_of_the_recently_added_team_member;
	
	@FindBy(id="modifyButton") 
	WebElement modify_button;
	
	@FindBy(id="email") 
	WebElement email_id_text_box;
	
	@FindBy(id="name") 
	WebElement Clear_Team_Member_Name;
	
	@FindBy(xpath="//input[@value='Save'][@type='button']") 
	WebElement modify_save_button;
	
	@FindBy(id="deleteButton") 
	WebElement delete_button_of_team_setup;
	
	@FindBy(id="totalLeaves") 
	WebElement total_leaves_text_box;
	
	
	public TeamSetUpPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void click_on_add_button_of_team_setup(String name, String mobile)
	{
//		add_button_of_team_setup.click();
		team_member_text_box.sendKeys(name);
		mobile_number.sendKeys(mobile);
		
		
	}
	
	public void enter_team_member_name(String team_member_name)
	{
		team_member_text_box.sendKeys(team_member_name);
	}
	
	public void enter_team_mobile_number(String team_member_mobilenumber)
	{
		mobile_number.sendKeys(team_member_mobilenumber);
	}
	
	public void click_on_organization_unit_dropdown()
	{
		select_organization_unit_dropdown.click();
	}
	
	public void click_on__shift_dropdown()
	{
		select_shift_dropdown.click();
	}
	
	public void click_on_designation_dropdown()
	{
		select_designation_dropdown.click();
	}
	
	public void click_on_skill_dropdown()
	{
		select_skill_dropdown.click();
	}
	
	public void click_on_save_button_of_team_setup()
	{
		save_button_of_team_setup.click();
	}
	
	
	public void enter_organization_unit_name_in_search_box(String org_unit)
	{
		search_box_of_organization_unit_dropdown.sendKeys(org_unit);
	}
	
	public void click_on_employee_id()
	{
		employee_id.click();	
	}
	
	public void select_shift_from_select_shift_dropdown()
	{
		first_shift_of_select_shift_dropdown.click();	
	}
	
	public void enter_recently_added_team_member_in_search_box_of_team_setup_page(String team_member)
	{
		search_box_of_team_setup_page.sendKeys(team_member);
	}
	
	public String recently_added_team_member_name()
	{
		String team_member = name_of_the_recently_added_team_member.getText();
		
		return  team_member;
	}
	
	
	public void click_on_modify_button()
	{
		modify_button.click();
	}
	
	public void enter_email_id(String email)
	{
		email_id_text_box.sendKeys(email);
	}
	
	public void Clear_team_member_name()
	{
		Clear_Team_Member_Name.clear();
	}
	
	
	public void Enter_modified_team_member_name(String name)
	{
		Clear_Team_Member_Name.sendKeys(name);
	}
	
	
	
	
	public void Click_on_modified_save_button()
	{
		modify_save_button.click();
	}
	
	public void Click_delete_button()
	{
		delete_button_of_team_setup.click();
	}
	
	
	public void Enter_total_leaves_in_total_leaves_text_box(String total)
	{
		total_leaves_text_box.sendKeys(total);
	}


	public void addMail(String mail) {
		email.sendKeys(mail);
		
	}

	
	
}
