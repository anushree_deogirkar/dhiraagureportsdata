package com.qa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;


public class ReadConfig 
{
	Properties pro;

	public ReadConfig()
	{

	File src = new File("src\\main\\java\\com\\qa\\config\\config.properties");

	
	try
	{
		FileInputStream fis = new FileInputStream(src);
		pro = new Properties();
		pro.load(fis);
	}
	catch(Exception e)
	{
		System.out.println("Exception is "+e.getMessage());	
	}
	}


	public String getApplicationURL()
	{
		String URL = pro.getProperty("baseURL");
		return URL;
	}

	public String getUsername()
	{
		String username = pro.getProperty("username");
		return username;
	}

	public String getPassword()
	{
		String password = pro.getProperty("password");
		return password;
	}

	public String getChromePath()
	{
		String chromepath = pro.getProperty("chromepath");
		return chromepath;
	}

	public String getFirefoxPath()
	{
		String firefoxpath = pro.getProperty("firefoxpath");
		return firefoxpath;
	}
	
	public String get_organization_unit_id()
	{
		String organization_unit_id_text_box = pro.getProperty("organization_unit_id_text_box");
		return organization_unit_id_text_box;
	}
	
	public String get_organization_unit_name()
	{
		String organization_unit_name_textbox = pro.getProperty("organization_unit_name_textbox");
		return organization_unit_name_textbox;
	}
	
	public String get_organization_unit_address()
	{
		String organization_unit_address_textbox = pro.getProperty("organization_unit_address_textbox");
		return organization_unit_address_textbox;
	}
	
	public String get_Latitude()
	{
		String Latitude_textbox = pro.getProperty("Latitude_textbox");
		return Latitude_textbox;
	}
	
	public String get_Longitude()
	{
		String Longitude_textbox = pro.getProperty("Longitude_textbox");
		return Longitude_textbox;
	}
	
	public String get_Minimum_Tasks_Per_Team_Member_per_day()
	{
		String Minimum_Tasks_Per_Team_Member_per_day = pro.getProperty("Minimum_Tasks_Per_Team_Member_per_day");
		return Minimum_Tasks_Per_Team_Member_per_day;
	}
	
	public String get_recently_added_division_name()
	{
		String recently_added_division_name = pro.getProperty("recently_added_division_name");
		return recently_added_division_name;
	}
	
	
	
	
	
	public String get_modify_organization_unit_id_text_box()
	{
		String modify_organization_unit_id_text_box = pro.getProperty("modify_organization_unit_id_text_box");
		return modify_organization_unit_id_text_box;
	}
	
	public String get_modify_organization_unit_name_textbox()
	{
		String modify_organization_unit_name_textbox = pro.getProperty("modify_organization_unit_name_textbox");
		return modify_organization_unit_name_textbox;
	}
	
	public String get_modify_organization_unit_address_textbox()
	{
		String modify_organization_unit_address_textbox = pro.getProperty("modify_organization_unit_address_textbox");
		return modify_organization_unit_address_textbox;
	}
	
	public String get_modify_Latitude_textbox()
	{
		String modify_Latitude_textbox = pro.getProperty("modify_Latitude_textbox");
		return modify_Latitude_textbox;
	}
	
	public String get_modify_Longitude_textbox()
	{
		String modify_Longitude_textbox = pro.getProperty("modify_Longitude_textbox");
		return modify_Longitude_textbox;
	}
	
	
	public String get_modify_Minimum_Tasks_Per_Team_Member_per_day()
	{
		String modify_Minimum_Tasks_Per_Team_Member_per_day = pro.getProperty("modify_Minimum_Tasks_Per_Team_Member_per_day");
		return modify_Minimum_Tasks_Per_Team_Member_per_day;
	}
	
	
	public String get_modify_recently_added_division_name()
	{
		String modify_recently_added_division_name = pro.getProperty("modify_recently_added_division_name");
		return modify_recently_added_division_name;
	}
	
	public String get_modified_organization_unit_name_textbox()
	{
		String modified_organization_unit_name_textbox = pro.getProperty("modified_organization_unit_name_textbox");
		return modified_organization_unit_name_textbox;
	}
	
	public String get_delete_organization_unit_id_text_box()
	{
		String delete_organization_unit_id_text_box = pro.getProperty("delete_organization_unit_id_text_box");
		return delete_organization_unit_id_text_box;
	}
	
	public String get_delete_organization_unit_name_textbox()
	{
		String delete_organization_unit_name_textbox = pro.getProperty("delete_organization_unit_name_textbox");
		return delete_organization_unit_name_textbox;
	}
	
	public String get_delete_organization_unit_address_textbox()
	{
		String delete_organization_unit_address_textbox = pro.getProperty("delete_organization_unit_address_textbox");
		return delete_organization_unit_address_textbox;
	}
	
	public String get_delete_Minimum_Tasks_Per_Team_Member_per_day()
	{
		String delete_Minimum_Tasks_Per_Team_Member_per_day = pro.getProperty("delete_Minimum_Tasks_Per_Team_Member_per_day");
		return delete_Minimum_Tasks_Per_Team_Member_per_day;
	}
	
	public String get_delete_recently_added_division_name()
	{
		String delete_recently_added_division_name = pro.getProperty("delete_recently_added_division_name");
		return delete_recently_added_division_name;
	}
	
	public String get_Team_member_name()
	{
		String Team_member_name = pro.getProperty("Team_member_name");
		return Team_member_name;
	}
	
	public String get_Team_member_mobile_number()
	{
		String Team_member_mobile_number = pro.getProperty("Team_member_mobile_number");
		return Team_member_mobile_number;
	}
	
	
	public String get_search_organization_unit()
	{
		String search_organization_unit = pro.getProperty("search_organization_unit");
		return search_organization_unit;
	}
	
	
	
	public String get_modify_Team_member_name()
	{
		String modify_Team_member_name = pro.getProperty("modify_Team_member_name");
		return modify_Team_member_name;
	}
	
	
	public String get_modify_Team_member_mobile_number()
	{
		String modify_Team_member_mobile_number = pro.getProperty("modify_Team_member_mobile_number");
		return modify_Team_member_mobile_number;
	}
	
	
	public String get_modify_search_organization_unit()
	{
		String modify_search_organization_unit = pro.getProperty("modify_search_organization_unit");
		return modify_search_organization_unit;
	}
	
	public String get_modified_Team_Member_Name()
	{
		String modified_Team_Member_Name = pro.getProperty("modified_Team_Member_Name");
		return modified_Team_Member_Name;
	}
	
	
	
	
	
	
	public String get_delete_Team_member_name()
	{
		String delete_Team_member_name = pro.getProperty("delete_Team_member_name");
		return delete_Team_member_name;
	}
	
	
	public String get_delete_Team_member_mobile_number()
	{
		String delete_Team_member_mobile_number = pro.getProperty("delete_Team_member_mobile_number");
		return delete_Team_member_mobile_number;
	}
	
	
	
	public String get_delete_search_organization_unit()
	{
		String delete_search_organization_unit = pro.getProperty("delete_search_organization_unit");
		return delete_search_organization_unit;
	}
	
	
	
	
	
	public String get_manager_name()
	{
		String manager_name = pro.getProperty("manager_name");
		return manager_name;
	}
	
	public String get_manager_mobile_number()
	{
		String manager_mobile_number = pro.getProperty("manager_mobile_number");
		return manager_mobile_number;
	}
	
	
	public String get_manager_email_id()
	{
		String manager_email_id = pro.getProperty("manager_email_id");
		return manager_email_id;
	}
	
	
	public String get_manager_username()
	{
		String manager_username = pro.getProperty("manager_username");
		return manager_username;
	}
	
	public String get_organization_unit()
	{
		String organization_unit = pro.getProperty("organization_unit");
		return organization_unit;
	}
	
	
	
	
	public String get_enter_task_type_name()
	{
		String enter_task_type_name = pro.getProperty("enter_task_type_name");
		return enter_task_type_name;
	}
	
	public String get_enter_task_type_id()
	{
		String enter_task_type_id = pro.getProperty("enter_task_type_id");
		return enter_task_type_id;
	}
	
	
	public String get_enter_task_group_name_in_task_group_search_box()
	{
		String enter_task_group_name_in_task_group_search_box = pro.getProperty("enter_task_group_name_in_task_group_search_box");
		return enter_task_group_name_in_task_group_search_box;
	}
	
	
	
	
	
	public String get_modify_enter_task_type_name()
	{
		String modify_enter_task_type_name = pro.getProperty("modify_enter_task_type_name");
		return modify_enter_task_type_name;
	}
	
	public String get_modify_enter_task_type_id()
	{
		String modify_enter_task_type_id = pro.getProperty("modify_enter_task_type_id");
		return modify_enter_task_type_id;
	}
	
	
	public String get_modify_enter_task_group_name_in_task_group_search_box()
	{
		String modify_enter_task_group_name_in_task_group_search_box = pro.getProperty("modify_enter_task_group_name_in_task_group_search_box");
		return modify_enter_task_group_name_in_task_group_search_box;
	}
	
	public String get_modified_task_type_name()
	{
		String modified_task_type_name = pro.getProperty("modified_task_type_name");
		return modified_task_type_name;
	}
	
	
	
	public String get_delete_enter_task_type_name()
	{
		String delete_enter_task_type_name = pro.getProperty("delete_enter_task_type_name");
		return delete_enter_task_type_name;
	}
	
	public String get_delete_enter_task_type_id()
	{
		String delete_enter_task_type_id = pro.getProperty("delete_enter_task_type_id");
		return delete_enter_task_type_id;
	}
	
	public String get_delete_enter_task_group_name_in_task_group_search_box()
	{
		String delete_enter_task_group_name_in_task_group_search_box = pro.getProperty("delete_enter_task_group_name_in_task_group_search_box");
		return delete_enter_task_group_name_in_task_group_search_box;
	}
	
	
	
	
	
	public String get_task_action_name()
	{
		String task_action_name = pro.getProperty("task_action_name");
		return task_action_name;
	}
	
	public String get_benchmark_time_of_task_action()
	{
		String baenchmark_time = pro.getProperty("baenchmark_time");
		return baenchmark_time;
	}
	
	
	
	
	
	public String get_modify_task_action_name()
	{
		String modify_task_action_name = pro.getProperty("modify_task_action_name");
		return modify_task_action_name;
	}
	
	public String get_modify_baenchmark_time()
	{
		String modify_baenchmark_time = pro.getProperty("modify_baenchmark_time");
		return modify_baenchmark_time;
	}
	
	public String get_modify_search_organization_unit_of_task_action()
	{
		String modify_search_organization_unit = pro.getProperty("modify_search_organization_unit");
		return modify_search_organization_unit;
	}
	
	public String get_modified_task_action_name()
	{
		String modified_task_action_name = pro.getProperty("modified_task_action_name");
		return modified_task_action_name;
	}
	
	
	
	public String get_task_workflow_name()
	{
		String task_workflow_name = pro.getProperty("task_workflow_name");
		return task_workflow_name;
	}
	
	public String get_task_type_to_search()
	{
		String task_type_to_search = pro.getProperty("task_type_to_search");
		return task_type_to_search;
	}
	
	public String get_state_and_city_to_search()
	{
		String state_and_city_to_search = pro.getProperty("state_and_city_to_search");
		return state_and_city_to_search;
	}
	
	public String get_organization_unit_to_search()
	{
		String organization_unit_to_search = pro.getProperty("organization_unit_to_search");
		return organization_unit_to_search;
	}
	
	public String get_average_completion_time()
	{
		String average_completion_time = pro.getProperty("average_completion_time");
		return average_completion_time;
	}
	
	
	
	
	
	
	public String get_title()
	{
		String title = pro.getProperty("title");
		return title;
	}
	
	public String get_type()
	{
		String type = pro.getProperty("type");
		return type;
	}
	
	public String get_order_number()
	{
		String order_number = pro.getProperty("order_number");
		return order_number;
	}
	
	public String get_customer_name()
	{
		String customer_name = pro.getProperty("customer_name");
		return customer_name;
	}
	
	public String get_contact_number()
	{
		String contact_number = pro.getProperty("contact_number");
		return contact_number;
	}
	
	public String get_state()
	{
		String state = pro.getProperty("state");
		return state;
	}
	
	public String get_city()
	{
		String city = pro.getProperty("city");
		return city;
	}
	
	public String get_address()
	{
		String address = pro.getProperty("address");
		return address;
	}
	
	
	public String get_organization_unit_of_task()
	{
		String organization_unit = pro.getProperty("organization_unit");
		return organization_unit;
	}
	
	
	
	
	
	
	
	
	
	public String get_department_organization_unit_id()
	{
		String department_organization_unit_id_text_box = pro.getProperty("department_organization_unit_id_text_box");
		return department_organization_unit_id_text_box;
	}
	
	
	public String get_department_organization_unit_name()
	{
		String department_organization_unit_name_textbox = pro.getProperty("department_organization_unit_name_textbox");
		return department_organization_unit_name_textbox;
	}
	
	
	public String get_department_organization_unit_address()
	{
		String department_organization_unit_address_textbox = pro.getProperty("department_organization_unit_address_textbox");
		return department_organization_unit_address_textbox;
	}
	
	
	public String get_department_Minimum_Tasks_Per_Team_Member_per_day_of_department()
	{
		String department_Minimum_Tasks_Per_Team_Member_per_day_of_department = pro.getProperty("department_Minimum_Tasks_Per_Team_Member_per_day_of_department");
		return department_Minimum_Tasks_Per_Team_Member_per_day_of_department;
	}
	
	
	public String get_department_recently_added_department_name()
	{
		String department_recently_added_department_name = pro.getProperty("department_recently_added_department_name");
		return department_recently_added_department_name;
	}
	
	
	
	
	
	public String get_section_organization_unit_id_text_box()
	{
		String section_organization_unit_id_text_box = pro.getProperty("section_organization_unit_id_text_box");
		return section_organization_unit_id_text_box;
	}
	
	public String get_section_organization_unit_name_textbox()
	{
		String section_organization_unit_name_textbox = pro.getProperty("section_organization_unit_name_textbox");
		return section_organization_unit_name_textbox;
	}
	
	public String get_section_organization_unit_address_textbox()
	{
		String section_organization_unit_address_textbox = pro.getProperty("section_organization_unit_address_textbox");
		return section_organization_unit_address_textbox;
	}
	
	public String get_section_Minimum_Tasks_Per_Team_Member_per_day_of_department()
	{
		String section_Minimum_Tasks_Per_Team_Member_per_day_of_department = pro.getProperty("section_Minimum_Tasks_Per_Team_Member_per_day_of_department");
		return section_Minimum_Tasks_Per_Team_Member_per_day_of_department;
	}
	
	public String get_section_recently_added_section_name()
	{
		String section_recently_added_section_name = pro.getProperty("section_recently_added_section_name");
		return section_recently_added_section_name;
	}
	
	
	
	
	
	
	public String get_unit_organization_unit_id()
	{
		String unit_organization_unit_id = pro.getProperty("unit_organization_unit_id");
		return unit_organization_unit_id;
	}
	
	public String get_unit_organization_unit_name()
	{
		String unit_organization_unit_name = pro.getProperty("unit_organization_unit_name");
		return unit_organization_unit_name;
	}
	
	public String get_unit_organization_unit_address()
	{
		String unit_organization_unit_address = pro.getProperty("unit_organization_unit_address");
		return unit_organization_unit_address;
	}
	
	public String get_unit_Minimum_Tasks_Per_Team_Member_per_day_of_department()
	{
		String unit_Minimum_Tasks_Per_Team_Member_per_day_of_department = pro.getProperty("unit_Minimum_Tasks_Per_Team_Member_per_day_of_department");
		return unit_Minimum_Tasks_Per_Team_Member_per_day_of_department;
	}
	
	public String get_unit_recently_added_section_name()
	{
		String unit_recently_added_section_name = pro.getProperty("unit_recently_added_section_name");
		return unit_recently_added_section_name;
	}
	
	
	
	
	
	
	public String get_first_task_workflow_name()
	{
		String first_task_workflow_name = pro.getProperty("first_task_workflow_name");
		return first_task_workflow_name;
	}
	
	public String get_first_task_type_to_search()
	{
		String first_task_type_to_search = pro.getProperty("first_task_type_to_search");
		return first_task_type_to_search;
	}
	
	public String get_first_state_and_city_to_search()
	{
		String first_state_and_city_to_search = pro.getProperty("first_state_and_city_to_search");
		return first_state_and_city_to_search;
	}
	
	public String get_first_one_organization_unit_to_search()
	{
		String first_one_organization_unit_to_search = pro.getProperty("first_one_organization_unit_to_search");
		return first_one_organization_unit_to_search;
	}
	
	public String get_first_second_organization_unit_to_search()
	{
		String first_second_organization_unit_to_search = pro.getProperty("first_second_organization_unit_to_search");
		return first_second_organization_unit_to_search;
	}
	
	public String get_first_one_average_completion_time()
	{
		String first_one_average_completion_time = pro.getProperty("first_one_average_completion_time");
		return first_one_average_completion_time;
	}
	
	public String get_first_second_average_completion_time()
	{
		String first_second_average_completion_time = pro.getProperty("first_second_average_completion_time");
		return first_second_average_completion_time;
	}
	
	
	public String get_koc_cpg_unit_manager_username()
	{
		String koc_cpg_unit_manager_username = pro.getProperty("koc_cpg_unit_manager_username");
		return koc_cpg_unit_manager_username;
	}
	
	public String get_koc_cpg_unit_manager_password()
	{
		String koc_cpg_unit_manager_password = pro.getProperty("koc_cpg_unit_manager_password");
		return koc_cpg_unit_manager_password;
	}
	public String get_koc_cpg_unit_team_member_name()
	{
		String team_member_name = pro.getProperty("team_member_name");
		return team_member_name;
	}
	
	
	
	
	
	public String get_delete_task_action_name()
	{
		String delete_task_action_name = pro.getProperty("delete_task_action_name");
		return delete_task_action_name;
	}
	
	public String get_delete_baenchmark_time()
	{
		String delete_baenchmark_time = pro.getProperty("delete_baenchmark_time");
		return delete_baenchmark_time;
	}
	

	
	

	public String get_Add_workflow_name()
	{
		String Add_workflow_name = pro.getProperty("Add_workflow_name");
		return Add_workflow_name;
	}
	
	

	public String get_Add_task_type_to_search()
	{
		String Add_task_type_to_search = pro.getProperty("Add_task_type_to_search");
		return Add_task_type_to_search;
	}
	
	
	
	
	public String get_modify_workflow_name()
	{
		String modify_workflow_name = pro.getProperty("modify_workflow_name");
		return modify_workflow_name;
	}
	
	

	public String get_modify_task_type_to_search()
	{
		String modify_task_type_to_search = pro.getProperty("modify_task_type_to_search");
		return modify_task_type_to_search;
	}
	
	
	
	public String get_modified_workflow_name()
	{
		String modified_workflow_name = pro.getProperty("modified_workflow_name");
		return modified_workflow_name;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	public String get_Delete_workflow_name()
	{
		String Delete_workflow_name = pro.getProperty("Delete_workflow_name");
		return Delete_workflow_name;
	}
	

	public String get_Delete_task_type_to_search()
	{
		String Delete_task_type_to_search = pro.getProperty("Delete_task_type_to_search");
		return Delete_task_type_to_search;
	}
	
	
	
	
	
	
	
	public String get_modify_manager_name()
	{
		String modify_manager_name = pro.getProperty("modify_manager_name");
		return modify_manager_name;
	}
	
	public String get_modify_manager_mobile_number()
	{
		String modify_manager_mobile_number = pro.getProperty("modify_manager_mobile_number");
		return modify_manager_mobile_number;
	}
	
	public String get_modify_manager_email_id()
	{
		String modify_manager_email_id = pro.getProperty("modify_manager_email_id");
		return modify_manager_email_id;
	}
	
	public String get_modify_manager_username()
	{
		String modify_manager_username = pro.getProperty("modify_manager_username");
		return modify_manager_username;
	}
	
	public String get_modify_organization_unit()
	{
		String modify_organization_unit = pro.getProperty("modify_organization_unit");
		return modify_organization_unit;
	}
	
	public String get_modified_manager_name()
	{
		String modified_manager_name = pro.getProperty("modified_manager_name");
		return modified_manager_name;
	}
	
	
	
	
	
	public String get_delete_manager_name()
	{
		String delete_manager_name = pro.getProperty("delete_manager_name");
		return delete_manager_name;
	}
	
	public String get_delete_manager_mobile_number()
	{
		String delete_manager_mobile_number = pro.getProperty("delete_manager_mobile_number");
		return delete_manager_mobile_number;
	}
	
	public String get_delete_manager_email_id()
	{
		String delete_manager_email_id = pro.getProperty("delete_manager_email_id");
		return delete_manager_email_id;
	}
	
	public String get_delete_manager_username()
	{
		String delete_manager_username = pro.getProperty("delete_manager_username");
		return delete_manager_username;
	}
	
	public String get_delete_organization_unit()
	{
		String delete_organization_unit = pro.getProperty("delete_organization_unit");
		return delete_organization_unit;
	}
	
	
	
	
	
	
	
	
	
	
	public String get_modify_organization_unit_of_task()
	{
		String modify_organization_unit = pro.getProperty("modify_organization_unit");
		return modify_organization_unit;
	}
	
	public String get_modify_title()
	{
		String modify_title = pro.getProperty("modify_title");
		return modify_title;
	}
	
	public String get_modify_type()
	{
		String modify_type = pro.getProperty("modify_type");
		return modify_type;
	}
	
	public String get_modify_order_number()
	{
		String modify_order_number = pro.getProperty("modify_order_number");
		return modify_order_number;
	}
	
	public String get_modify_customer_name()
	{
		String modify_customer_name = pro.getProperty("modify_customer_name");
		return modify_customer_name;
	}
	
	public String get_modify_contact_number()
	{
		String modify_contact_number = pro.getProperty("modify_contact_number");
		return modify_contact_number;
	}
	
	public String get_modify_state()
	{
		String modify_state = pro.getProperty("modify_state");
		return modify_state;
	}
	
	public String get_modify_city()
	{
		String modify_city = pro.getProperty("modify_city");
		return modify_city;
	}
	
	public String get_modify_address()
	{
		String modify_address = pro.getProperty("modify_address");
		return modify_address;
	}
	
	
	public String get_modify_team_member_name()
	{
		String modify_team_member_name = pro.getProperty("modify_team_member_name");
		return modify_team_member_name;
	}
	
	
	public String get_modified_customer_name()
	{
		String modified_customer_name = pro.getProperty("modified_customer_name");
		return modified_customer_name;
	}
	
	
	
	
	
	
	

	public String get_delete_organization_unit_of_task()
	{
		String delete_organization_unit = pro.getProperty("delete_organization_unit");
		return delete_organization_unit;
	}
	
	public String get_delete_title()
	{
		String delete_title = pro.getProperty("delete_title");
		return delete_title;
	}
	
	public String get_delete_type()
	{
		String delete_type = pro.getProperty("delete_type");
		return delete_type;
	}
	
	public String get_delete_order_number()
	{
		String delete_order_number = pro.getProperty("delete_order_number");
		return delete_order_number;
	}
	
	public String get_delete_customer_name()
	{
		String delete_customer_name = pro.getProperty("delete_customer_name");
		return delete_customer_name;
	}
	
	public String get_delete_contact_number()
	{
		String delete_contact_number = pro.getProperty("delete_contact_number");
		return delete_contact_number;
	}
	
	public String get_delete_state_of_task()
	{
		String delete_state = pro.getProperty("delete_state");
		return delete_state;
	}
	
	public String get_delete_city()
	{
		String delete_city = pro.getProperty("delete_city");
		return delete_city;
	}
	
	public String get_delete_address()
	{
		String delete_address = pro.getProperty("delete_address");
		return delete_address;
	}
	
	
	public String get_search_organization_units()
	{
		String search_organization_units = pro.getProperty("search_organization_units");
		return search_organization_units;
	}
	
	
	public String get_modify_Total_Leaves()
	{
		String modify_Total_Leaves = pro.getProperty("modify_Total_Leaves");
		return modify_Total_Leaves;
	}	
	
	
	
	
	
	
	public String eoc_cpg_unit_manager_username()
	{
		String eoc_cpg_unit_manager_username = pro.getProperty("eoc_cpg_unit_manager_username");
		return eoc_cpg_unit_manager_username;
	}
	
	public String eoc_cpg_unit_manager_password()
	{
		String eoc_cpg_unit_manager_password = pro.getProperty("eoc_cpg_unit_manager_password");
		return eoc_cpg_unit_manager_password;
	}
	
	public String eoc_lpins_unit_manager_username()
	{
		String eoc_lpins_unit_manager_username = pro.getProperty("eoc_lpins_unit_manager_username");
		return eoc_lpins_unit_manager_username;
	}
	
	
	public String eoc_lpins_unit_manager_password()
	{
		String eoc_lpins_unit_manager_password = pro.getProperty("eoc_lpins_unit_manager_password");
		return eoc_lpins_unit_manager_password;
	}
	
	

	public String Task_type_of_scenario1()
	{
		String Task_type_of_scenario1 = pro.getProperty("Task_type_of_scenario1");
		return Task_type_of_scenario1;
	}
	
	public String order_number_of_scenario1()
	{
		String order_number_of_scenario1 = pro.getProperty("order_number_of_scenario1");
		return order_number_of_scenario1;
	}
	
	public String city_of_scenario1()
	{
		String city_of_scenario1 = pro.getProperty("city_of_scenario1");
		return city_of_scenario1;
	}
	
	
	
}
