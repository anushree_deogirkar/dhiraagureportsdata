package com.qa.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.qa.base.BaseClass;

public class TestUtil extends BaseClass {

	public static long page_load_timeout=80;
	public static long implicit_wait=80;
	public static String TESTDATA_SHEET_PATH="src/main/java/com/qa/testData/LoginTestDataNew.xlsx";
	static org.apache.poi.ss.usermodel.Workbook book;
	static Sheet sheet;
	 static Sheet Sheet1 = null;
	public static int totalSheet = 0;
	static Object[][] data;
	
	
	public static Object[][] getTestData(String sheetName,String pageName) {
		
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		totalSheet=book.getNumberOfSheets();
		
		if (totalSheet > 0) {
            
            for (int k = 0; k < totalSheet; k++) {
              String sheetFetched=  book.getSheetName(k);
                if(sheetFetched.equalsIgnoreCase(pageName)) {
                	sheet=book.getSheetAt(k);
            		 data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
            		
            		for (int i = 0; i < sheet.getLastRowNum(); i++) {
            			for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {
            				data[i][j] = sheet.getRow(i + 1).getCell(j).toString();
            				 System.out.println(data[i][j]);
            			}
            		}
                }else
                {
                	System.out.println("PageName not matched with sheet");
                }
              
//                Sheet1 = book.getNumberOfSheets();
                		
                System.out.println("Sheet Found:" + Sheet1);
		}
		
		}
		
		
		return data;
	}
	
	


	

public static void takeScreenshot() throws IOException {
	File sourceFile=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	String currentDirectory=System.getProperty("user.dir");
	FileUtils.copyFile(sourceFile, new File(currentDirectory+"/Screenshot/"+System.currentTimeMillis()+".png"));
}






public static void getSheetFromWorkbook(String excelSheetName) {
	FileInputStream file = null;
	try {
		file = new FileInputStream(TESTDATA_SHEET_PATH);
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	}
	try {
		book = WorkbookFactory.create(file);
	} catch (InvalidFormatException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
	
	if (totalSheet > 0) {
        for (int k = 0; k < totalSheet; k++) {
            Sheet1 = book.getSheetAt(k);
	}
	
	}
	
}
}
