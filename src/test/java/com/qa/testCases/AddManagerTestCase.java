package com.qa.testCases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Home_Page;
import com.qa.pages.Login;
import com.qa.pages.LoginPage;
import com.qa.pages.Manager;
import com.qa.utils.ReadConfig;
import com.qa.utils.TestUtil;


public class AddManagerTestCase extends BaseClass {

	HomePage homePage;
	Login login;
	Manager manager;
	String excelSheetName = "LoginTestData";

	public AddManagerTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		login = new Login();
		homePage = new HomePage();
//		manager = new Manager();

	}

	
	@Test()

	public void addManagerTestCase() throws InterruptedException, IOException {
//		driver.manage().deleteAllCookies();
		ReadConfig readConfig  = new ReadConfig();

		String username = "rajat.zade@locationguru.com";
		String password = "tech8092";

		String manager_name = readConfig.get_manager_name();
		String manager_mobile_umber = readConfig.get_manager_mobile_number();


		String manager_email_id = readConfig.get_manager_email_id();

		String manager_username = readConfig.get_manager_username();

		String org_unit = readConfig.get_organization_unit();





		WebDriverWait wait = new WebDriverWait(driver, 30);

//		logger.info("URL is opened");

		LoginPage login_page = new LoginPage(driver);



		login_page.Enter_username("rajat.zade@locationguru.com");
//		logger.info("Entered username");

		login_page.Enter_password("tech8092");
		
//		logger.info("Entered password");

		login_page.click_on_login_button();
//		logger.info("Clicked on login buton");


		driver.manage().window().maximize();




		Home_Page home_page = new Home_Page(driver);

		home_page.click_on_admin_button();

		home_page.click_on_Manager();

		Manager manager = new Manager(driver);


		driver.findElement(By.xpath("//input[@type='button'][@value='Add']")).click();

		manager.enter_manager_name(manager_name);



		manager.click_on_select_organization_unit_dropdown();

		manager.enter_organization_unit(org_unit);
Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/form/div[1]/div[4]/div/div/div/div/div/ul/li/ul/li[2]/ul/li[3]/ul/li[5]/a")).click();

		manager.click_on_manager_name();





		manager.enter_manager_mobile_number(manager_mobile_umber);

		manager.enter_manager_emailid(manager_email_id);

		manager.enter_manager_username(manager_username);


		driver.findElement(By.id("check-username-availability")).click();


		manager.click_on_save_button();

		Thread.sleep(3000);





		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='Search']")));


		manager.enter_manager_name_in_search_box(manager_name);

		Thread.sleep(2000);

		/*

		String manager_Name = manager.capture_manager_name();
		System.out.println(manager_Name);


		Assert.assertEquals("rudra oberoi", manager_Name);

		 */



		//Parent Organization unit type drop-down
		String before_xpath2 = "//table[@id='managerTable']//tbody/tr[";
		String after_xpath2 = "]/td[2]/div[1]";

		List<WebElement> Name_of_org_unit = driver.findElements(By.xpath("//table[@id='managerTable']//tbody/tr/td[2]/div[1]"));

		int count_of_Name_of_org_unit = Name_of_org_unit.size();

		System.out.println(count_of_Name_of_org_unit);


		for(int i=1;i<=count_of_Name_of_org_unit;i++)
		{
//			logger.info("Entered in first for loop");

			String d2 = before_xpath2+i+after_xpath2;
			String org_unit_name = driver.findElement(By.xpath(d2)).getText();

//			logger.info("Available parent organization unit :"+org_unit_name);

			Thread.sleep(500);

			if (org_unit_name.contains(manager_name)) 
			{ 
//				Assert.assertEquals(division_name, expected);

				break;
			}
		}
}
	@AfterMethod
	public void closeDriver() {
		driver.close();

	}
}
