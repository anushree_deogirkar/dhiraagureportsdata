package com.qa.testCases;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskTypePage;
import com.qa.pages.TeamSetUpPage;
import com.qa.utils.TestUtil;

public class AddTeamSetUpTestCase extends BaseClass {
	HomePage homePage;
	Login login;
	TeamSetUpPage teamSetupPage;
	String excelSheetName = "LoginTestDataNew";

	public AddTeamSetUpTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		login = new Login();
		homePage = new HomePage();
		teamSetupPage = new TeamSetUpPage();

	}

	@DataProvider
	
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "Teams");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")

	public void addTeamSetUpTestCase(String name, String mobile,String org,String mail) throws InterruptedException, IOException {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String user = properties.getProperty("username");
		String pswrd = properties.getProperty("password");

		homePage = login.login(user, pswrd);
		homePage.click_on_admin_button();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		homePage.click_on_Team_Setup();

		driver.findElement(By.xpath("/html/body/div[1]/section/div[1]/form/div[2]/div[3]/span/input[2]")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"email\"]")).sendKeys(mail);

//		teamSetupPage.addMail(mail);
		teamSetupPage.click_on_add_button_of_team_setup(name, mobile);
		teamSetupPage.click_on_organization_unit_dropdown();
		String search_organization_unit = org;
		teamSetupPage.enter_organization_unit_name_in_search_box(search_organization_unit);
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/div/form/div/div/div[1]/div[3]/div[3]/div[1]/div/div/ul/li/ul/li/ul/li/ul/li/a")));
		Thread.sleep(1000);
		driver.findElement(By.xpath(
				"/html/body/div[1]/section/div/form/div/div/div[1]/div[3]/div[3]/div[1]/div/div/ul/li/ul/li[2]/ul/li[3]/ul/li[5]/div"))
				.click();
//		Thread.sleep(2000);
//
//		driver.findElement(By.xpath(
//				"/html/body/div[1]/section/div/form/div/div/div[1]/div[3]/div[3]/div[1]/div/div/ul/li/ul/li/ul/li/ul/li/a"))
//				.click();
//		Thread.sleep(2000);

//		driver.findElement(By.xpath(
//				"/html/body/div[1]/section/div/form/div/div/div[1]/div[3]/div[3]/div[1]/div/div/ul/li/ul/li/ul/li/ul/li/a"))
//				.click();
		Thread.sleep(1000);
		teamSetupPage.click_on_employee_id();
		Thread.sleep(1000);
		
//		driver.findElement(By.id("totalLeaves")).sendKeys("4");
		teamSetupPage.click_on__shift_dropdown();
		teamSetupPage.select_shift_from_select_shift_dropdown();
//		Thread.sleep(5000);
		teamSetupPage.click_on_save_button_of_team_setup();
		Thread.sleep(2000);
		driver.findElement(By.xpath("/html/body/div[10]/div[7]/div/button")).click();
		Thread.sleep(2000);
		
		teamSetupPage.enter_recently_added_team_member_in_search_box_of_team_setup_page(name);
		Thread.sleep(1000);

		// Parent Organization unit type drop-down
		String before_xpath2 = "//table[@id='teamTable']//tbody/tr/td[";
		String after_xpath2 = "]/div[1]";
		List<WebElement> Name_of_org_unit = driver
				.findElements(By.xpath("//table[@id='teamTable']//tbody/tr/td[2]/div[1]/*"));
		 int count_of_Name_of_org_unit = Name_of_org_unit.size();
		System.out.println(count_of_Name_of_org_unit);
		for (int i = 1; i <= count_of_Name_of_org_unit; i++) {
			String d2 = before_xpath2 + i + after_xpath2;
			String org_unit_name = driver.findElement(By.xpath(d2)).getText();
			Thread.sleep(500);

			if (org_unit_name.contains(name)) {
//				 Assert.assertEquals(name, name);

				break;
			
		}
		
//		open mail to read password
//		driver.get("http://webmail.locationguru.com/");
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.id("io-ox-login-username"))).sendKeys(mail);
//		driver.findElement(By.id("io-ox-login-password")).sendKeys(password);
//		Thread.sleep(2000);
//		driver.findElement(By.id("io-ox-login-button")).click();
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.id("io-ox-top-logo-small"))).isDisplayed();
//		driver.navigate().refresh();
//		Thread.sleep(2000);
//		
//		
//		driver.findElement(By.xpath("//input[@placeholder='Search...']")).sendKeys("Teampilot Notification");
		
//		driver.findElement(By.xpath("//*[@id=\"1574839685734177-tokenfield\"]")).sendKeys("Teampilot Notification");
//		Thread.sleep(3000);
		
//		driver.findElement(By.xpath("//body")).sendKeys(Keys.RETURN);

		
//		driver.findElement(By.xpath("//input[@placeholder='Search...']")).sendKeys(Keys.RETURN);//
//		Thread.sleep(3000);
//		
//		driver.findElement(By.xpath("//*[@id=\"io.ox/mail\"]/ul/li[2]")).click();
//        System.out.println("SIZE>>>"+a.size());
//        for (int j = 0; j < a.size(); j++) {
//            System.out.println(a.get(j).getText());
//            if (a.get(j).getText().equals("Support")) //to click on a specific mail.
//                {                                           
//                a.get(j).click();
//            }
        }
		}
		

	
}
