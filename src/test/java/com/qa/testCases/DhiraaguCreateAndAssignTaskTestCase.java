package com.qa.testCases;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.utils.TestUtil;

public class DhiraaguCreateAndAssignTaskTestCase extends BaseClass {

	HomePage homePage;
	Login login;
	TaskPage taskPage;
	String excelSheetName = "LoginTestData";
 
	public DhiraaguCreateAndAssignTaskTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		login = new Login();
		homePage = new HomePage();
		taskPage = new TaskPage();

	}

	@DataProvider
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "TaskAssign");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")


	public void testCase01(String title, String orgUnit,String order,String type,String customer,String phone,
			String addr,String state,String city,  String member,String assigntime,String status)
	
					throws InterruptedException, IOException, ClassNotFoundException, SQLException {
		String uid = null;

		String user = properties.getProperty("username");
		String pswrd = properties.getProperty("password");
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
//		driver.manage().deleteAllCookies();
		// Print the Date
		homePage = login.login(user, pswrd);
		homePage.click_on_admin_button();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		homePage.click_on_Task_Menu();
		taskPage.click_on_add_button_of_task();
		Thread.sleep(3000);
		taskPage.click_on_organization_unit_dropdown();
		Thread.sleep(1000);
		taskPage.enter_org_unit_in_organization_unit_dropdown(orgUnit);
		Thread.sleep(1000);
//		driver.findElement(By.xpath("//*[@id=\"serviceCentreId\"]/div/ul/li[1]")).click();
//		driver.findElement(By.xpath("//*[@id=\"serviceCentreId\"]/div/ul/li")).click();

		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("taskAddBtn")));

		String before_xpath11 = "//div[@id='serviceCentreId']//ul/li[";
		String after_xpath11 = "]";

		List<WebElement> count_of_or_unit = driver.findElements(By.xpath("//div[@id='serviceCentreId']//ul/li"));
		//
		int Count_of_or_unit = count_of_or_unit.size();
		////
		for (int i = 1; i <= Count_of_or_unit; i++) {
			String d = before_xpath11 + i + after_xpath11;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();
			////
			//// // logger.info("Available unit Name :"+org_unit_name);
			////
			Thread.sleep(500);
			////
			if (org_unit_name.contains(orgUnit)) {
				Thread.sleep(500);

				driver.findElement(By.xpath(before_xpath11 + i + after_xpath11)).click();

				break;

			}
		}
		driver.findElement(By.xpath("//*[@id=\"order-desc\"]")).sendKeys(title);

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@id='taskTypeId']//span[@class='placeholder']")));

		taskPage.click_on_type_dropdown();
		driver.findElement(By.xpath("//*[@id=\"taskTypeId\"]/a[1]/span/span"));
		taskPage.enter_type_in_search_box_of_type_drop_down(type);
		Thread.sleep(2000);
		//
		String before_xpath1 = "//div[@id='taskTypeId']//div[@class='dropdown-main']//ul/li[";
		String after_xpath1 = "]";

		List<WebElement> count_of_task_type = driver
				.findElements(By.xpath("//div[@id='taskTypeId']//div[@class='dropdown-main']//ul/li"));
		//
		int Count_of_task_type = count_of_task_type.size();

		System.out.println(Count_of_task_type);

		for (int i = 1; i <= Count_of_task_type; i++) {
			// logger.info("Entered in first for loop");

			String d = before_xpath1 + i + after_xpath1;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			// logger.info("Available unit Name :"+org_unit_name);

			Thread.sleep(700);

			if (org_unit_name.contains(type)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(before_xpath1 + i + after_xpath1)));

				driver.findElement(By.xpath(before_xpath1 + i + after_xpath1)).click();

				break;

			}
		}
		driver.findElement(By.xpath("//*[@id=\"clientServiceOrderId\"]")).sendKeys(order);

		driver.findElement(By.xpath("//*[@id=\"clientName\"]")).sendKeys(customer);
		//
		driver.findElement(By.xpath("//*[@id=\"contactNumber\"]")).sendKeys(phone);
		 //
		taskPage.click_on_state_dropdown();
		// logger.info("clicked_on_state_dropdown");
		//
		WebElement ele = driver.findElement(By.xpath("//*[@id=\"stateMultiselectId\"]/div/span/input"));

		String before_xpath2 = "//div[@id='stateMultiselectId']//ul/li[";
		String after_xpath2 = "]";

		List<WebElement> count_of_state = driver.findElements(By.xpath("//div[@id='stateMultiselectId']//ul/li"));

		int Count_of_state = count_of_state.size();

		System.out.println(Count_of_state);

		for (int i2 = 1; i2 <= Count_of_state; i2++) {
			// logger.info("Entered in first for loop");

			String d2 = before_xpath2 + i2 + after_xpath2;
			String org_unit_name2 = driver.findElement(By.xpath(d2)).getText();
//
			// logger.info("Available state Name :"+org_unit_name);

			Thread.sleep(500);
			//
			if (org_unit_name2.contains(state)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(before_xpath2 + i2 + after_xpath2)));

				driver.findElement(By.xpath(before_xpath2 + i2 + after_xpath2)).click();

				break;

			}
		}
//
//		// Set priority
//
//		 taskPage.click_on_state_dropdown();
//		 logger.info("clicked_on_state_dropdown");
//
		driver.findElement(By.xpath("//*[@id=\"filter-priority\"]/a[1]/span/span")).click();
		;
		Thread.sleep(1000);
////
////		//
		WebElement priority = driver.findElement(By.xpath("//*[@id=\"filter-priority\"]/div/span/input"));
		priority.sendKeys("P1");
Thread.sleep(1000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"filter-priority\"]/div/ul/li")));

		driver.findElement(By.xpath("//li[@data-value='1']")).click();
		Thread.sleep(1000);
		

		taskPage.click_on_city_dropdown();
		// // logger.info("clicked_on_city_dropdown");
		//
		taskPage.enter_city_in_search_box_of_city_dropdown(city);
		// logger.info("entered_city_in_search_box_of_city_dropdown");
Thread.sleep(1000);
		String before_xpath3 = "//div[@id='cityMultiselectId']//ul/li[";
		String after_xpath3 = "]";
//
		List<WebElement> count_of_city = driver.findElements(By.xpath("//div[@id='cityMultiselectId']//ul/li"));
//
		int Count_of_city = count_of_state.size();
//
		System.out.println(Count_of_city);
//
		for (int i1 = 1; i1 <= Count_of_city; i1++) {
			// logger.info("Entered in first for loop");

			String d1 = before_xpath3 + i1 + after_xpath3;
			String org_unit_name1 = driver.findElement(By.xpath(d1)).getText();
//			 
//			// logger.info("Available city Name :"+org_unit_name);
//
			Thread.sleep(1000);
//
			if (org_unit_name1.contains(city)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(before_xpath3 + i1 + after_xpath3)));

				driver.findElement(By.xpath("//li[@data-value='"+city+"']")).click();
				
//				driver.findElement(By.xpath(before_xpath3 + i1 + after_xpath3)).click();

				break;

			}
		}
		// WebDriverWait wait = new WebDriverWait(driver, 30);

		taskPage.enter_address_in_address_text_box(addr);

		Thread.sleep(1000);
		
		taskPage.save_button_of_task_page();
		Thread.sleep(4000);
		driver.findElement(By.xpath("//button[@class='confirm']")).click();
		Thread.sleep(4000);
		// .visibilityOfElementLocated(By.xpath("//span[contains(text(),'Tasks')]")));
//		wait.until(ExpectedConditions
//				.visibilityOfElementLocated(By.xpath("//li[@id='manageTask']")));
//	driver.findElement(By.xpath("/html/body/div[1]/aside/div/nav/ul/li[4]/a")).click();
//		driver.navigate().refresh();
//	Thread.sleep(5000);
	
//	driver.findElement(By.xpath("/html/body/div[1]/aside/div/nav/ul/li[4]/a")).click();
	homePage.click_on_Task_Menu();

	Thread.sleep(2000);

	taskPage.click_on_tile_view();
	Thread.sleep(2000);
	taskPage.click_on_list_view();

//	taskPage.click_on_tile_view();

	Thread.sleep(2000);

	taskPage.click_on_All_button();

	Thread.sleep(2000);

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//input[@id='taskList-search-bar']")))
				.sendKeys(title);
		;
		Thread.sleep(3000);
		driver.findElement(
				By.xpath("//i[@class='fa fa-pencil']"))
				.click();
		
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")).sendKeys(assigntime);
//		 
//		wait.until(ExpectedConditions.visibilityOfElementLocated(
//				By.xpath("/html/body/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div/div[1]/div/span/input")));
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@id='multipleSelect-assignedTo-taskList-modal']/div[@class='dropdown-display-label']/div[@class='dropdown-chose-list']/span[@class='dropdown-search']/input[@placeholder='Search']"))
				.sendKeys(member);

		Thread.sleep(2000);
		////
//		 wait.until(ExpectedConditions
//		 .visibilityOfElementLocated(By.xpath("//li[@class='dropdown-option  employee-workStatus']")));

		 driver.findElement(By.xpath("/html/body/div[9]/div/div/div[2]/div/div[2]/div[2]/div[2]/div/div[2]/ul/li")).click();
		 
		 Thread.sleep(2000);
		 driver.findElement(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")).click();
		taskPage.clickOnAssign();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='confirm']")).click();
		//
		Thread.sleep(1000);


	}

}