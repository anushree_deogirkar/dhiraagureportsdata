package com.qa.testCases;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.utils.TestUtil;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class MarkAttendanceTestCase extends BaseClass {

	HomePage homePage;
	Login login;
	TaskPage taskPage;
	String excelSheetName = "LoginTestData";

	public MarkAttendanceTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
//		initialization();
		login = new Login();
		homePage = new HomePage();
		taskPage = new TaskPage();

	}

	@DataProvider
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "TeamPilotAttendance");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")
	public void testCase01(String username, String password, String punchInTime, String punchOutTime)
			throws InterruptedException {

		String punchInJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchInTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Europe\\/Kiev \",\r\n" + "   \"event\":10,\r\n"
				+ "   \"latitude\":50.522406 ,\r\n" + "   \"longitude\":30.618102,\r\n"
				+ "   \"distFromPlace\":\"1974424.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		String punchOutJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchOutTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Europe\\/Kiev \",\r\n" + "   \"event\":40,\r\n"
				+ "   \"latitude\":50.522406 ,\r\n" + "   \"longitude\":30.618102,\r\n"
				+ "   \"distFromPlace\":\"1974424.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		// Login members
		Response postResponseLogin = given()
				.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/authentication/login?username="
						+username+ "&password=" + password + "");

		System.out.println("Logoin code>>" + postResponseLogin.getStatusCode());
		assertEquals(postResponseLogin.getStatusCode(), 200);
		// System.out.println(postResponseLogin.getHeader("Authorization"));

		// Punch IN

//		Thread.sleep(1000);
		Response postResponsePunchIn = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
				.contentType(ContentType.JSON).body(punchInJSON).when()
				.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/attendance" + "");
		assertEquals(postResponsePunchIn.getStatusCode(), 200);

		 System.out.println("PunchIN Code>>"+postResponsePunchIn.getStatusCode());
		//
		// Punch OUT
//		Thread.sleep(1000);
		Response postResponsePunchOut = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
				.contentType(ContentType.JSON).body(punchOutJSON).when()
				.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/attendance" + "");
		System.out.println(punchOutTime);
		assertEquals(postResponsePunchOut.getStatusCode(), 200);

		 System.out.println("Punch Out Code>>"+postResponsePunchIn.getStatusCode());
		//
		// Logout
//		Thread.sleep(1000);
		Response postResponseLogout = given()
				.post("\r\n" + "https://demoapp.teampilot.co/loginAction.do?action=logOut" + "");
		assertEquals(postResponseLogout.getStatusCode(), 200);

		////
		 System.out.println("Logout Code>>"+postResponsePunchIn.getStatusCode());
		//

	}

	
}
