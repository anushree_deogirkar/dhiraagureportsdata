package com.qa.testCases;

import static org.testng.Assert.assertEquals;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import io.restassured.matcher.RestAssuredMatchers;
import io.restassured.path.json.JsonPath;

import org.json.JSONArray;
import org.json.JSONException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;
import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.utils.TestUtil;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class MemberLoginAndStatusChangeTestCase extends BaseClass {
	HomePage homePage;
	Login login;
	TaskPage taskPage;
	String excelSheetName = "LoginTestData";

	public MemberLoginAndStatusChangeTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		// initialization();
		login = new Login();
		homePage = new HomePage();
		taskPage = new TaskPage();

	}
	
	@DataProvider
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "ChangeStatusTeamPilot");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")

	// public void testCase01(String punchInTime,
	// String punchoutTime, String username, String password, String assignedTime,
	// String acknowledgeTime,
	// String travelStartedTime, String checkInTime, String jobStartedTime, String
	// completedTime
	// )
	public void testCase01(String details, String member, String username, String password, String punchIntime,
			String acknowledgeTime, String travelStartedTime, String checkInTime, String jobStartedTime,
			String completedTime, String rejectedTime, String suspendedTime, String waitlistedTime, String punchoutTime)
			throws InterruptedException, IOException, ClassNotFoundException, SQLException, JSONException {

		String uid = null;
		String punchInJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchIntime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Europe\\/Kviy\",\r\n" + "   \"event\":10,\r\n"
				+ "   \"latitude\":50.4502991,\r\n" + "   \"longitude\":30.5187097,\r\n"
				+ "   \"distFromPlace\":\"10.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

//		String assignedJson = "{\r\n" + "    \"taskStatus\": \"769\",\r\n" + "    \"comments\": \" \",\r\n"
//				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + assignTime + "\"" + "\r\n"
//				+ "}";

		String acknowledgedJson = "{\r\n" + "    \"taskStatus\": \"776\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + acknowledgeTime + "\""
				+ "\r\n" + "}";

		String travelStartedJson = "{\r\n" + "    \"taskStatus\": \"779\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + travelStartedTime + "\""
				+ "\r\n" + "}";

		String checkInJson = "{\r\n" + "    \"taskStatus\": \"771\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + checkInTime + "\"" + "\r\n"
				+ "}";

		String jobStartedJson = "{\r\n" + "    \"taskStatus\": \"780\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + jobStartedTime + "\""
				+ "\r\n" + "}";

		String completedJson = "{\r\n" + "    \"taskStatus\": \"774\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + completedTime + "\""
				+ "\r\n" + "}";

		String suspendedJson = "{\r\n" + "    \"taskStatus\": \"735\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + suspendedTime + "\""
				+ "\r\n" + "}";

		String rejectedJson = "{\r\n" + "    \"taskStatus\": \"737\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + rejectedTime + "\""
				+ "\r\n" + "}";

		String waitlistedJson = "{\r\n" + "    \"taskStatus\": \"736\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + waitlistedTime + "\""
				+ "\r\n" + "}";
		//
		String punchOutJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchoutTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Europe\\/Kviy\",\r\n" + "   \"event\":40,\r\n"
				+ "   \"latitude\":50.4502991,\r\n" + "   \"longitude\":30.5187097,\r\n"
				+ "   \"distFromPlace\":\"9.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		// Login Team member
		Response postResponseLogin = given()
				.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/authentication/login?username=" + username
						+ "&password=" + password + "");

		// System.out.println("Logoin code>>" + postResponseLogin.getStatusCode());
		assertEquals(postResponseLogin.getStatusCode(), 200);
		// System.out.println("Login?>>" + postResponseLogin.getStatusCode());
		// System.out.println("AuthO?>>" +
		// postResponseLogin.getHeader("Authorization"));

		//
		// Punch IN
		// System.out.println(postResponseLogin.getHeader("Authorization"));

		Response postResponsePunchIn = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
				.contentType(ContentType.JSON).body(punchInJSON).when()
				.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/attendance" + "");
		assertEquals(postResponsePunchIn.getStatusCode(), 200);
		// System.out.println("punch in?>>" + postResponsePunchIn.getStatusCode());

		try {
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://demoapp.teampilot.co:5435/kyvistar",
					"postgres", "Tech8092");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from task where title=" + "'" + details + "'");
			while (rs.next()) {
				 System.out.println(rs.getString(64));
				uid = rs.getString(64);
				Response postAcknowledgeStatus = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(acknowledgedJson).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/modify-status/" + uid);
				assertEquals(postAcknowledgeStatus.getStatusCode(), 200);
				// System.out.println("Post assigned ?>>" +
				// postAcknowledgeStatus.getStatusCode());
				//
				//// // Changing status from acknowledge to travel started
				Response postTravelStartedStatus = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(travelStartedJson).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/modify-status/" + uid);
				System.out.println(postTravelStartedStatus.getStatusCode());
				assertEquals(postTravelStartedStatus.getStatusCode(), 200);
				// System.out.println("Post assigned ?>>" +
				// postTravelStartedStatus.getStatusCode());
				////
				// // Changing status from travel started to Check In
				Response postCheckInStatus = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(checkInJson).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/modify-status/" + uid);
				assertEquals(postCheckInStatus.getStatusCode(), 200);
				System.out.println("Post assigned ?>>" + postCheckInStatus.getStatusCode());
				////
				//// // Changing status from Check In to Job started
				Response postJobstartedStatus = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(jobStartedJson).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/modify-status/" + uid);
				assertEquals(postJobstartedStatus.getStatusCode(), 200);
				System.out.println("Post assigned ?>>" + postJobstartedStatus.getStatusCode());
				////
				//
				//// if(title.contains("Complete")) {
				Response postCompletedStatus = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(completedJson).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/modify-status/" + uid);
				assertEquals(postCompletedStatus.getStatusCode(), 200);
				System.out.println("Post assigned ?>>" + postCompletedStatus.getStatusCode());

				System.out.println(postResponseLogin.getHeader("Authorization"));

				Response postResponsePunchOut = given()
						.header("Authorization", postResponseLogin.getHeader("Authorization"))
						.contentType(ContentType.JSON).body(punchOutJSON).when()
						.post("\r\n" + "https://demoapp.teampilot.co/web-api/api/attendance" + "");
				assertEquals(postResponsePunchOut.getStatusCode(), 200);
				// System.out.println("punch out?>>"+postResponsePunchOut.getStatusCode());
				//
				//// LogOut Team Member
				// // Logout
				Response postResponseLogout = given()
						.post("\r\n" + "https://demoapp.teampilot.co/loginAction.do?action=logOut" + "");
				assertEquals(postResponseLogout.getStatusCode(), 200);

			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		// System.out.println(postResponseLogin.getHeader("Authorization"));
		//// Get count of tasks assigned
		// Response postResponseGetTasks = given().header("Authorization",
		// postResponseLogin.getHeader("Authorization"))
		// .contentType(ContentType.JSON).when()
		// .post("\r\n" + "https://demoapp.teampilot.co/web-api/api/tasks/search" + "");
		// assertEquals(postResponseGetTasks.getStatusCode(), 200);
		//
		// int taskCount=postResponseGetTasks.jsonPath().getList("results").size();
		//// System.out.println("taskCount"+taskCount);
		//
		// JsonPath jsonPathValidator = postResponseGetTasks.jsonPath();
		//
		//
		//

		//

		//
		// Changing status from assigned to acknowledge

		// }
		// if(title.contains("Suspend")) {
		// Response postCompletedStatus = given().header("Authorization",
		// postResponseLogin.getHeader("Authorization"))
		// .contentType(ContentType.JSON).body(suspendedJson).when()
		// .post("\r\n" +
		// "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/"
		// +taskUID);
		// assertEquals(postCompletedStatus.getStatusCode(), 200);
		// }
		// if(title.contains("Reject")) {
		// Response postCompletedStatus = given().header("Authorization",
		// postResponseLogin.getHeader("Authorization"))
		// .contentType(ContentType.JSON).body(rejectedJson).when()
		// .post("\r\n" +
		// "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/"
		// +taskUID);
		// assertEquals(postCompletedStatus.getStatusCode(), 200);
		// }
		// if(title.contains("Waitlist")) {
		// Response postCompletedStatus = given().header("Authorization",
		// postResponseLogin.getHeader("Authorization"))
		// .contentType(ContentType.JSON).body(waitlistedJson).when()
		// .post("\r\n" +
		// "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/"
		// +taskUID);
		// assertEquals(postCompletedStatus.getStatusCode(), 200);
		// }

		//// // Changing status from Job started to completed

		//
		//
		// }
		//
		// // punch out team member

	}

}
