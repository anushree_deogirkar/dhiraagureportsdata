package com.qa.testCases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.pages.TaskWorkflowPage;
import com.qa.utils.TestUtil;

public class MultiWorkflowMultiDeptTestCase extends BaseClass {

	HomePage homePage;
	Login login;
	TaskWorkflowPage taskWorkflowPage;
	TaskPage taskPage;
	String excelSheetName = "LoginTestData";

	public MultiWorkflowMultiDeptTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		login = new Login();
		homePage = new HomePage();
		taskWorkflowPage = new TaskWorkflowPage();
		taskPage = new TaskPage();
	}

	@DataProvider
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "MultiDeptMultiWorkflowData");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")

	public void testCase01(String name, String taskType, String org,String teamMember,String username,String password,String title,String order,String customer,String contact,
			String state,String cityNew,String address,String assignTime ,String punchInTime,String loginTime,String acknowledgeTime,String travelStartedTime,String checkInTime,String jobStartedTime,String completedTime,String status
			,String punchOutTime,String department,String org2,String org3) throws InterruptedException, IOException {
		
//		Jsons
		String punchInJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchInTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Asia\\/Kolkata\",\r\n" + "   \"event\":10,\r\n"
				+ "   \"latitude\":33.0842605,\r\n" + "   \"longitude\":44.508661,\r\n"
				+ "   \"distFromPlace\":\"1974424.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		String acknowledgedJson = "{\r\n" + "    \"taskStatus\": \"738\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + acknowledgeTime + "\""
				+ "\r\n" + "}";

		String travelStartedJson = "{\r\n" + "    \"taskStatus\": \"728\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + travelStartedTime + "\""
				+ "\r\n" + "}";

		String checkInJson = "{\r\n" + "    \"taskStatus\": \"729\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + checkInTime + "\"" + "\r\n"
				+ "}";

		String jobStartedJson = "{\r\n" + "    \"taskStatus\": \"730\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + jobStartedTime + "\""
				+ "\r\n" + "}";

		String completedJson = "{\r\n" + "    \"taskStatus\": \"732\",\r\n" + "    \"comments\": \" \",\r\n"
				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + completedTime + "\""
				+ "\r\n" + "}";
		
//		String suspendedJson = "{\r\n" + "    \"taskStatus\": \"735\",\r\n" + "    \"comments\": \" \",\r\n"
//				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + suspendedTime + "\""
//				+ "\r\n" + "}";
//		
//		String rejectedJson = "{\r\n" + "    \"taskStatus\": \"737\",\r\n" + "    \"comments\": \" \",\r\n"
//				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + rejectedTime + "\""
//				+ "\r\n" + "}";
//		
//		String waitlistedJson = "{\r\n" + "    \"taskStatus\": \"736\",\r\n" + "    \"comments\": \" \",\r\n"
//				+ "     \"commentCode\": null,\r\n" + "    \"statusUpdatedTime\": " + "\"" + waitlistedTime + "\""
//				+ "\r\n" + "}";
//
		String punchOutJSON = "{\r\n" + "   \"imeiNumber\":\"db99dfbc9610c868\",\r\n" + "   \"requestTime\":" + "\""
				+ punchOutTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Asia\\/Kolkata\",\r\n" + "   \"event\":40,\r\n"
				+ "   \"latitude\":55.0842605,\r\n" + "   \"longitude\":66.508661,\r\n"
				+ "   \"distFromPlace\":\"1974424.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		

		WebDriverWait wait = new WebDriverWait(driver, 30);
		String user = properties.getProperty("username");
		String pswrd = properties.getProperty("password");

		homePage = login.login(user, pswrd);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Admin')]")));
		homePage.click_on_admin_button();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(text(),'Organization Setup')]")));

		homePage.click_on_organization_setup();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("jobWorkflowsMenu")));

		driver.findElement(By.id("jobWorkflowsMenu")).click();
		taskWorkflowPage.click_on_Add_new_workflow_button(name);

		// taskWorkflowPage.click_on_task_type_dropdown();
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"taskTypeBlock\"]/div/button/span")));

			driver.findElement(By.xpath("//*[@id=\"taskTypeBlock\"]/div/button/span")).click();
			String before_xpath = "//div[@id='taskTypeBlock']//ul/li[";
			String after_xpath = "]/label";

			String a = "//div[@id='taskTypeBlock']//ul/li[";
			String b = "]/label/input";
			List<WebElement> org_unit = driver.findElements(By.xpath("//div[@id='taskTypeBlock']//ul/li/*"));

			int count_of_org = org_unit.size();

			System.out.println(count_of_org);
			for (int i = 1; i <= count_of_org; i++) {
				String d = before_xpath + i + after_xpath;
				String org_unit_name = driver.findElement(By.xpath(d)).getText();
				Thread.sleep(1000);

				if (org_unit_name.contains(taskType)) {
					Thread.sleep(1000);

					driver.findElement(By.xpath(a + i + b)).click();

					break;

				}
			}
		
		
		
		////
		taskWorkflowPage.click_on_state_cities_dropdown();

		String before_xpath2 = "//div[@id='selectStateCityBlock']//ul/li[";
		String after_xpath2 = "]/label";

		String a2 = "//div[@id='selectStateCityBlock']//ul/li[";
		String b2 = "]/label/input";

		List<WebElement> city = driver.findElements(By.xpath("//div[@id='selectStateCityBlock']//ul/li/*"));

		int City = city.size();

		System.out.println(City);

		for (int i = 1; i <= City; i++) {

			String d = before_xpath2 + i + after_xpath2;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains("Bathala - AA")) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a2 + i + b2)).click();

				break;

			}
		}
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//div[@class='workFlDepartmentFilterblock']//input[@placeholder='Search']"))
				.sendKeys(org);
		String before_xpath3 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String after_xpath3 = "]/label";

		String a3 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String b3 = "]/label/input";

		List<WebElement> org_unit1 = driver
				.findElements(By.xpath("//div[@class='workFlDepartmentFilterblock']//ul/li/*"));

		int Org_unit = org_unit1.size();


		for (int i = 1; i <= Org_unit; i++) {

			String d = before_xpath3 + i + after_xpath3;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains(org)) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a3 + i + b3)).click();

				break;

			}
		}
		
		
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[2]/div/div[1]/div[2]/div/div/div[1]/input"))
				.sendKeys(org2);
		String before_xpath4 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String after_xpath4 = "]/label";

		String a4 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String b4 = "]/label/input";

		List<WebElement> org_unit4 = driver
				.findElements(By.xpath("//div[@class='workFlDepartmentFilterblock']//ul/li/*"));

		int Org_unit4 = org_unit4.size();


		for (int i = 1; i <= Org_unit4; i++) {

			String d = before_xpath4 + i + after_xpath4;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains(org2)) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a4 + i + b4)).click();

				break;

			}
		}
		
		
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[3]/div/div[1]/div[2]/div/div/div[1]/input"))
				.sendKeys(org3);
		String before_xpath5 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String after_xpath5 = "]/label";

		String a5 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String b5 = "]/label/input";

		List<WebElement> org_unit5 = driver
				.findElements(By.xpath("//div[@class='workFlDepartmentFilterblock']//ul/li/*"));

		int Org_unit5 = org_unit5.size();


		for (int i = 1; i <= Org_unit5; i++) {

			String d = before_xpath5 + i + after_xpath5;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains(org3)) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a5 + i + b5)).click();

				break;

			}
		}
}
}
