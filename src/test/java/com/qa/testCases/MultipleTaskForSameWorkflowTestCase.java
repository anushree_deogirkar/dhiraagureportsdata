package com.qa.testCases;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.utils.TestUtil;

public class MultipleTaskForSameWorkflowTestCase extends BaseClass {HomePage homePage;
Login login;
TaskPage taskPage;
String excelSheetName = "LoginTestData";

public MultipleTaskForSameWorkflowTestCase() {
	super();
}

@BeforeMethod
public void setUp() {
	initialization();
	login = new Login();
	homePage = new HomePage();
	taskPage = new TaskPage();

}

@DataProvider
public Object[][] getAddManagerData() {
	Object testData[][] = TestUtil.getTestData(excelSheetName, "MultipleTaskSameWorkflowData");
	return testData;
}


}
