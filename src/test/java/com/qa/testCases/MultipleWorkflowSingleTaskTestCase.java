package com.qa.testCases;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.BaseClass;
import com.qa.pages.HomePage;
import com.qa.pages.Login;
import com.qa.pages.TaskPage;
import com.qa.pages.TaskWorkflowPage;
import com.qa.utils.TestUtil;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class MultipleWorkflowSingleTaskTestCase extends BaseClass {

	HomePage homePage;
	Login login;
	TaskWorkflowPage taskWorkflowPage;
	TaskPage taskPage;
	String excelSheetName = "LoginTestData";

	public MultipleWorkflowSingleTaskTestCase() {
		super();
	}

	@BeforeMethod
	public void setUp() {
		initialization();
		login = new Login();
		homePage = new HomePage();
		taskWorkflowPage = new TaskWorkflowPage();
		taskPage = new TaskPage();
	}

	@DataProvider
	public Object[][] getAddManagerData() {
		Object testData[][] = TestUtil.getTestData(excelSheetName, "MultiDeptOneTask");
		return testData;
	}

	@Test(dataProvider = "getAddManagerData")

	public void testCase01(String name, String taskType, String org, String teamMember, String username,
			String password, String title, String order, String customer, String contact, String state, String cityNew,
			String address, String assignTime, String punchInTime, String loginTime, String acknowledgeTime,
			String travelStartedTime, String checkInTime, String jobStartedTime, String completedTime, String status,
			String punchOutTime, String manager01User, String manager1Password, String manager02User,
			String manager2Password, String manager03User, String manager3Password, String approved01Time,
			String acknowledgeTime2, String travelStartedTime2, String checkInTime2, String jobStartedTime2,
			String completedTime2, String approved02Time, String acknowledgeTime3, String travelStartedTime3,
			String checkInTime3, String jobStartedTime3, String completedTime3, String approved03Time)
			throws InterruptedException, IOException {
		String uid = null;
		punchInTime = "2019-12-09T08:30:25.954+05:30";
		// Jsons
		String punchInJSON = "{\r\n" + "   \"imeiNumber\":\"610d79c7fdcb6c47\",\r\n" + "   \"requestTime\":" + "\""
				+ punchInTime + "\"" + ",\r\n" + "   \"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ "   \"status\":\"Y\",\r\n" + "   \"tz\":\"Asia\\/Kolkata\",\r\n" + "   \"event\":10,\r\n"
				+ "   \"latitude\":21.0842605,\r\n" + "   \"longitude\":73.508661,\r\n"
				+ "   \"distFromPlace\":\"1974424.3274435366\",\r\n" + "   \"notes\":\"\" \r\n" + "}";

		String acknowledgedJson = "{\r\n" + " \"taskStatus\": \"738\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + acknowledgeTime + "\"" + "\r\n"
				+ "}";

		String travelStartedJson = "{\r\n" + " \"taskStatus\": \"728\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + travelStartedTime + "\"" + "\r\n"
				+ "}";

		String checkInJson = "{\r\n" + " \"taskStatus\": \"729\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + checkInTime + "\"" + "\r\n" + "}";

		String jobStartedJson = "{\r\n" + " \"taskStatus\": \"730\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + jobStartedTime + "\"" + "\r\n"
				+ "}";

		String completedJson = "{\r\n" + " \"taskStatus\": \"732\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + completedTime + "\"" + "\r\n"
				+ "}";

		String manager1ApprovedJson = "{\r\n" + " \"taskStatus\": \"733\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + approved01Time + "\"" + "\r\n"
				+ "}";

		// Org2
		String acknowledgedJson2 = "{\r\n" + " \"taskStatus\": \"738\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + acknowledgeTime2 + "\"" + "\r\n"
				+ "}";
		String travelStartedJson2 = "{\r\n" + " \"taskStatus\": \"728\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + travelStartedTime2 + "\"" + "\r\n"
				+ "}";

		String checkInJson2 = "{\r\n" + " \"taskStatus\": \"729\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + checkInTime2 + "\"" + "\r\n"
				+ "}";

		String jobStartedJson2 = "{\r\n" + " \"taskStatus\": \"730\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + jobStartedTime2 + "\"" + "\r\n"
				+ "}";

		String completedJson2 = "{\r\n" + " \"taskStatus\": \"732\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + completedTime2 + "\"" + "\r\n"
				+ "}";

		String manager1ApprovedJson2 = "{\r\n" + " \"taskStatus\": \"733\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + approved02Time + "\"" + "\r\n"
				+ "}";

		// Org3
		// Org2
		String acknowledgedJson3 = "{\r\n" + " \"taskStatus\": \"738\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + acknowledgeTime3 + "\"" + "\r\n"
				+ "}";
		String travelStartedJson3 = "{\r\n" + " \"taskStatus\": \"728\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + travelStartedTime3 + "\"" + "\r\n"
				+ "}";

		String checkInJson3 = "{\r\n" + " \"taskStatus\": \"729\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + checkInTime3 + "\"" + "\r\n"
				+ "}";

		String jobStartedJson3 = "{\r\n" + " \"taskStatus\": \"730\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + jobStartedTime3 + "\"" + "\r\n"
				+ "}";

		String completedJson3 = "{\r\n" + " \"taskStatus\": \"732\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + completedTime3 + "\"" + "\r\n"
				+ "}";

		String manager1ApprovedJson3 = "{\r\n" + " \"taskStatus\": \"733\",\r\n" + "\"comments\": \" \",\r\n"
				+ " \"commentCode\": null,\r\n" + " \"statusUpdatedTime\": " + "\"" + approved03Time + "\"" + "\r\n"
				+ "}";

		String punchOutJSON = "{\r\n" + " \"imeiNumber\":\"610d79c7fdcb6c47\",\r\n" + " \"requestTime\":" + "\""
				+ punchOutTime + "\"" + ",\r\n" + "\"locTime\":\"2019-10-21T11:57:25.954+05:30\",\r\n"
				+ " \"status\":\"Y\",\r\n" + " \"tz\":\"Asia\\/Kolkata\",\r\n" + "\"event\":40,\r\n"
				+ " \"latitude\":55.0842605,\r\n" + " \"longitude\":66.508661,\r\n"
				+ " \"distFromPlace\":\"1974424.3274435366\",\r\n" + " \"notes\":\"\" \r\n" + "}";
		//
		WebDriverWait wait = new WebDriverWait(driver, 30);
		String user = properties.getProperty("username");
		String pswrd = properties.getProperty("password");

		homePage = login.login(user, pswrd);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Admin')]")));
		homePage.click_on_admin_button();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//li[@id='organizationSetup']")).click();

		driver.findElement(By.id("jobWorkflowsMenu")).click();
		taskWorkflowPage.click_on_Add_new_workflow_button(name);
		wait.until(
				ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"taskTypeBlock\"]/div/button/span")));

		driver.findElement(By.xpath("//*[@id=\"taskTypeBlock\"]/div/button/span")).click();
		String before_xpath = "//div[@id='taskTypeBlock']//ul/li[";
		String after_xpath = "]/label";

		String a = "//div[@id='taskTypeBlock']//ul/li[";
		String b = "]/label/input";
		List<WebElement> org_unit = driver.findElements(By.xpath("//div[@id='taskTypeBlock']//ul/li/*"));
		int count_of_org = org_unit.size();
		System.out.println(count_of_org);
		for (int i = 1; i <= count_of_org; i++) {
			String d = before_xpath + i + after_xpath;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();
			Thread.sleep(1000);

			if (org_unit_name.contains(taskType)) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a + i + b)).click();

				break;

			}
		}
		taskWorkflowPage.click_on_state_cities_dropdown();

		String before_xpath2 = "//div[@id='selectStateCityBlock']//ul/li[";
		String after_xpath2 = "]/label";

		String a2 = "//div[@id='selectStateCityBlock']//ul/li[";
		String b2 = "]/label/input";

		List<WebElement> city = driver.findElements(By.xpath("//div[@id='selectStateCityBlock']//ul/li/*"));

		int City = city.size();

		System.out.println(City);

		for (int i = 1; i <= City; i++) {
			//
			String d = before_xpath2 + i + after_xpath2;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains("Bathala - AA")) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a2 + i + b2)).click();

				break;

			}
		}
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//div[@class='workFlDepartmentFilterblock']//input[@placeholder='Search']"))
				.sendKeys(org);
		String before_xpath3 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String after_xpath3 = "]/label";

		String a3 = "//div[@class='workFlDepartmentFilterblock']//ul/li[";
		String b3 = "]/label/input";

		List<WebElement> org_unit1 = driver
				.findElements(By.xpath("//div[@class='workFlDepartmentFilterblock']//ul/li/*"));

		int Org_unit = org_unit1.size();

		for (int i = 1; i <= Org_unit; i++) {

			String d = before_xpath3 + i + after_xpath3;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			Thread.sleep(1000);

			if (org_unit_name.contains(org)) {
				Thread.sleep(1000);

				driver.findElement(By.xpath(a3 + i + b3)).click();

				break;

			}
		}
		taskWorkflowPage.click_on_task_workflow_name();
		// Another org unit
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[2]/div/div[1]/div[2]/div/div/div[1]/input"))
				.sendKeys("Networks Division");

		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[2]/div/div[1]/div[2]/div/div/ul/li[38]/label"))
				.click();

		taskWorkflowPage.click_on_task_workflow_name();

		// 3rd org unit
		taskWorkflowPage.click_on_add_organization_unit_button();
		taskWorkflowPage.click_on_select_organization_unit_dropdown();
		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[3]/div/div[1]/div[2]/div/div/div[1]/input"))
				.sendKeys("Complaint Department");

		driver.findElement(By.xpath("//*[@id=\"assignContainer\"]/div[3]/div/div[1]/div[2]/div/div/ul/li[79]/label"))
				.click();

		taskWorkflowPage.click_on_task_workflow_name();
		taskWorkflowPage.click_on_save_button_of_task_workflow();
		Thread.sleep(3000);

		if (driver.findElement(By.xpath("//button[@class='confirm']")).isDisplayed()) {
			driver.findElement(By.xpath("//button[@class='confirm']")).click();
		}

		Thread.sleep(3000);

		homePage.click_on_Task_Menu();
		taskPage.click_on_add_button_of_task();
		Thread.sleep(2000);
		taskPage.click_on_organization_unit_dropdown();
		Thread.sleep(1000);
		taskPage.enter_org_unit_in_organization_unit_dropdown(org);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//*[@id=\"serviceCentreId\"]/div/ul/li[1]")).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("taskAddBtn")));

		String before_xpath11 = "//div[@id='serviceCentreId']//ul/li[";
		String after_xpath11 = "]";

		List<WebElement> count_of_or_unit = driver.findElements(By.xpath("//div[@id='serviceCentreId']//ul/li"));
		int Count_of_or_unit = count_of_or_unit.size();
		for (int i = 1; i <= Count_of_or_unit; i++) {
			String d = before_xpath11 + i + after_xpath11;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();
			Thread.sleep(500);
			if (org_unit_name.contains(org)) {
				Thread.sleep(500);

				driver.findElement(By.xpath(before_xpath11 + i + after_xpath11)).click();

				break;

			}
		}
		driver.findElement(By.xpath("//*[@id=\"order-desc\"]")).sendKeys(title);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//div[@id='taskTypeId']//span[@class='placeholder']")));

		taskPage.click_on_type_dropdown();
		driver.findElement(By.xpath("//*[@id=\"taskTypeId\"]/a[1]/span/span"));
		taskPage.enter_type_in_search_box_of_type_drop_down(taskType);
		Thread.sleep(2000);
		String before_xpath1 = "//div[@id='taskTypeId']//div[@class='dropdown-main']//ul/li[";
		String after_xpath1 = "]";

		List<WebElement> count_of_task_type = driver
				.findElements(By.xpath("//div[@id='taskTypeId']//div[@class='dropdown-main']//ul/li"));
		int Count_of_task_type = count_of_task_type.size();

		System.out.println(Count_of_task_type);

		for (int i = 1; i <= Count_of_task_type; i++) {
			// logger.info("Entered in first for loop");

			String d = before_xpath1 + i + after_xpath1;
			String org_unit_name = driver.findElement(By.xpath(d)).getText();

			// logger.info("Available unit Name :"+org_unit_name);

			Thread.sleep(700);

			if (org_unit_name.contains(taskType)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(before_xpath1 + i + after_xpath1)));

				driver.findElement(By.xpath(before_xpath1 + i + after_xpath1)).click();

				break;

			}
		}
		driver.findElement(By.xpath("//*[@id=\"clientServiceOrderId\"]")).sendKeys(order);

		driver.findElement(By.xpath("//*[@id=\"clientName\"]")).sendKeys(customer);
		//
		driver.findElement(By.xpath("//*[@id=\"contactNumber\"]")).sendKeys(contact);

		taskPage.click_on_state_dropdown();
		WebElement ele = driver.findElement(By.xpath("//*[@id=\"stateMultiselectId\"]/div/span/input"));

		String before_xpath0 = "//div[@id='stateMultiselectId']//ul/li[";
		String after_xpath0 = "]";
		//
		List<WebElement> count_of_state = driver.findElements(By.xpath("//div[@id='stateMultiselectId']//ul/li"));

		int Count_of_state = count_of_state.size();

		System.out.println(Count_of_state);

		for (int i2 = 1; i2 <= Count_of_state; i2++) {
			// logger.info("Entered in first for loop");

			String d2 = before_xpath0 + i2 + after_xpath0;
			String org_unit_name2 = driver.findElement(By.xpath(d2)).getText();

			// logger.info("Available state Name :"+org_unit_name);

			Thread.sleep(500);
			//
			if (org_unit_name2.contains(state)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(before_xpath0 + i2 + after_xpath0)));

				driver.findElement(By.xpath(before_xpath0 + i2 + after_xpath0)).click();

				break;

			}
		}
		driver.findElement(By.xpath("//*[@id=\"filter-priority\"]/a[1]/span/span")).click();
		;

		WebElement priority = driver.findElement(By.xpath("//*[@id=\"filter-priority\"]/div/span/input"));
		priority.sendKeys("P1");
		Thread.sleep(1000);

		driver.findElement(By.xpath("//li[@data-value='1']")).click();
		Thread.sleep(1000);
		taskPage.click_on_city_dropdown();
		taskPage.enter_city_in_search_box_of_city_dropdown(cityNew);
		Thread.sleep(1000);
		driver.findElement(By.xpath("//li[@data-value='" + cityNew + "']")).click();
		taskPage.enter_address_in_address_text_box(address);

		Thread.sleep(1000);

		taskPage.save_button_of_task_page();
		Thread.sleep(3000);
		driver.findElement(By.xpath("/html/body/div[18]/div[7]/div/button")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//li[@id='manageTask']")).click();
		Thread.sleep(3000);
		Thread.sleep(2000);
		taskPage.click_on_tile_view();
		Thread.sleep(2000);
		taskPage.click_on_list_view();
		Thread.sleep(2000);
		taskPage.click_on_All_button();
		Thread.sleep(2000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("/html/body/div[1]/section/div[6]/div[1]/div[2]/input")))
				.sendKeys(order);
		;
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@class='cursor-pointer text-info']")).click();

		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")));

		driver.findElement(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")).clear();
		driver.findElement(By.xpath("//*[@id=\"datetimepicker-appointment-taskList-input\"]")).sendKeys(assignTime);
		Thread.sleep(1000);
		driver.findElement(By.xpath(
				"//div[@id='multipleSelect-assignedTo-taskList-modal']/div[@class='dropdown-display-label']/div[@class='dropdown-chose-list']/span[@class='dropdown-search']/input[@placeholder='Search']"))
				.sendKeys(teamMember);

		Thread.sleep(2000);
		driver.findElement(
				By.xpath("//div[@id='multipleSelect-assignedTo-taskList-modal']/div[@class='dropdown-main']/ul/li"))
				.click();

		Thread.sleep(2000);
		taskPage.clickOnAssign();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@class='confirm']")).click();
		Thread.sleep(1000);
		// Change status of tasks
		Response postResponseLogin = given()
				.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/authentication/login?username="
						+ username + "&password=" + password + "");

		assertEquals(postResponseLogin.getStatusCode(), 200);
		//
		//// Punch IN
		Response postResponsePunchIn = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
				.contentType(ContentType.JSON).body(punchInJSON).when()
				.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/attendance" + "");
		assertEquals(postResponsePunchIn.getStatusCode(), 200);

		// Database connectivity
		try {
			Class.forName("org.postgresql.Driver");
			Connection con = DriverManager.getConnection("jdbc:postgresql://192.168.220.28:5434/dhiraagu_dev1_23oct",
					"lgro", "Tech8092");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select uid from task where title=" + "'" + title + "'");
			while (rs.next()) {
				System.out.println(rs.getString(1));
				uid = rs.getString(1);
			}

			// Assign to ack
			// Changing status from acknowledge to travel started
			Response postAckStatus = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(acknowledgedJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postAckStatus.getStatusCode());
			assertEquals(postAckStatus.getStatusCode(), 200);
			// Changing status from acknowledge to travel started
			Response postTravelStartedStatus = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(travelStartedJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postTravelStartedStatus.getStatusCode());
			assertEquals(postTravelStartedStatus.getStatusCode(), 200);

			// Changing status from travel started to Check In
			Response postCheckInStatus = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(checkInJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCheckInStatus.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCheckInStatus.getStatusCode());
			//
			// // Changing status from Check In to Job started
			Response postJobstartedStatus = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(jobStartedJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postJobstartedStatus.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postJobstartedStatus.getStatusCode());

			Response postCompletedStatus = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(completedJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCompletedStatus.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCompletedStatus.getStatusCode());

			// Manager 1 login
			Response postResponseManagerLogin = given()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/authentication/login?username="
							+ manager01User + "&password=" + manager1Password + "");

			System.out.println("Logoin code>>" + postResponseManagerLogin.getHeader("Authorization"));
			System.out.println(uid);
			assertEquals(postResponseManagerLogin.getStatusCode(), 200);
			// Approved by manager
			Response postApproved01Status = given()
					.header("Authorization", postResponseManagerLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(manager1ApprovedJson).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postApproved01Status.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postApproved01Status.getStatusCode());

			// Status change for 2nd org unit
			// Assign to ack
			// Changing status from acknowledge to travel started
			Response postAckStatus2 = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(acknowledgedJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postAckStatus2.getStatusCode());
			assertEquals(postAckStatus2.getStatusCode(), 200);
			// Changing status from acknowledge to travel started
			Response postTravelStartedStatus2 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(travelStartedJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postTravelStartedStatus2.getStatusCode());
			assertEquals(postTravelStartedStatus2.getStatusCode(), 200);

			// Changing status from travel started to Check In
			Response postCheckInStatus2 = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(checkInJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCheckInStatus2.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCheckInStatus2.getStatusCode());
			//
			// // Changing status from Check In to Job started
			Response postJobstartedStatus2 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(jobStartedJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postJobstartedStatus2.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postJobstartedStatus2.getStatusCode());

			Response postCompletedStatus2 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(completedJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCompletedStatus2.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCompletedStatus2.getStatusCode());

			// Approve by manager 2
			// Manager 2 login
			Response postResponseManagerLogin2 = given()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/authentication/login?username="
							+ manager02User + "&password=" + manager2Password + "");

			System.out.println("Logoin code>>" + postResponseManagerLogin2.getHeader("Authorization"));
			System.out.println(uid);
			assertEquals(postResponseManagerLogin2.getStatusCode(), 200);
			// Approved by manager
			Response postApproved02Status = given()
					.header("Authorization", postResponseManagerLogin2.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(manager1ApprovedJson2).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postApproved02Status.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postApproved02Status.getStatusCode());

			// Status change for 3nd org unit
			// Assign to ack
			// Changing status from acknowledge to travel started
			Response postAckStatus3 = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(acknowledgedJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postAckStatus3.getStatusCode());
			assertEquals(postAckStatus3.getStatusCode(), 200);
			// Changing status from acknowledge to travel started
			Response postTravelStartedStatus3 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(travelStartedJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			System.out.println(postTravelStartedStatus3.getStatusCode());
			assertEquals(postTravelStartedStatus3.getStatusCode(), 200);

			// Changing status from travel started to Check In
			Response postCheckInStatus3 = given().header("Authorization", postResponseLogin.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(checkInJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCheckInStatus3.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCheckInStatus3.getStatusCode());
			//
			// // Changing status from Check In to Job started
			Response postJobstartedStatus3 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(jobStartedJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postJobstartedStatus3.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postJobstartedStatus3.getStatusCode());

			Response postCompletedStatus3 = given()
					.header("Authorization", postResponseLogin.getHeader("Authorization")).contentType(ContentType.JSON)
					.body(completedJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postCompletedStatus3.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postCompletedStatus3.getStatusCode());

			// Approve by manager 2
			// Manager 2 login
			Response postResponseManagerLogin3 = given()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/authentication/login?username="
							+ manager03User + "&password=" + manager3Password + "");

			System.out.println("Logoin code>>" + postResponseManagerLogin3.getHeader("Authorization"));
			System.out.println(uid);
			assertEquals(postResponseManagerLogin3.getStatusCode(), 200);
			// Approved by manager
			Response postApproved03Status = given()
					.header("Authorization", postResponseManagerLogin3.getHeader("Authorization"))
					.contentType(ContentType.JSON).body(manager1ApprovedJson3).when()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/tasks/modify-status/" + uid);
			assertEquals(postApproved03Status.getStatusCode(), 200);
			System.out.println("Post assigned ?>>" + postApproved03Status.getStatusCode());
			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		
//		System.out.println(postResponseLogin.getHeader("Authorization"));
		
		Response postResponsePunchOut = given().header("Authorization",postResponseLogin.getHeader("Authorization") )
				.contentType(ContentType.JSON).body(
						punchOutJSON).when()
.post("\r\n" + "https://dev1.teampilot.locationguru.in/web-api/api/attendance" + "");
				assertEquals(postResponsePunchOut.getStatusCode(), 200);
//				System.out.println("punch out?>>"+postResponsePunchOut.getStatusCode());
//				
////LogOut Team Member
//				// Logout
			Response postResponseLogout = given()
					.post("\r\n" + "https://dev1.teampilot.locationguru.in/loginAction.do?action=logOut" + "");
			assertEquals(postResponseLogout.getStatusCode(), 200);

	}
}
